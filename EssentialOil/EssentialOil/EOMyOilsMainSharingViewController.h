//
//  EOMyOilsMainSharingViewController.h
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EOMyOilsShareListViewController.h"

@interface EOMyOilsMainSharingViewController : UIViewController<UITextViewDelegate>

@property int selectedContact;
@property NSString* selectedIndex; // This is the oil selected index
@property int unit;
@property UIViewController* parentVCX;

@property NSString *noteTitle;
@property NSString *noteDescription;

- (IBAction)noteSaved:(id)sender;
@end
