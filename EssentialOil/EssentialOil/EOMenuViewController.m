//
//  EOMenuViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMenuViewController.h"
#import "EOOilIndexViewController.h"
#import "EOCommonUseViewController.h"
#import "EODidyouknowViewController.h"
#import "EOEssentialOilBasicsDetailViewController.h"
#import "EOMyFavouriteViewController.h"
#import "EOMyOilsViewController.h"

@interface EOMenuViewController ()
@property int flag;
@end

@implementation EOMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onEssentialOilBasics:) name:@"Nav_EssentialOilBasics" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onCommonUses:) name:@"Nav_CommonUses" object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onDidYouKnow:) name:@"Nav_DidYouKnow" object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onMyFavourites:) name:@"Nav_MyFavourites" object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onMyOils:) name:@"Nav_MyOils" object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onOilIndex:) name:@"Nav_OilIndex" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onHome:) name:@"Nav_Home" object:nil];


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onEssentialOilBasics:(id)sender {
    UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"essentialbasicoildetailview"];
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)onMyOils:(id)sender {
    UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myoilsview"];
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)onOilIndex:(id)sender {
    UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"oilindexview"];
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)onCommonUses:(id)sender {
    UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"commonuseview"];
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)onMyFavourites:(id)sender {
    UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myfavourites"];
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)onDidYouKnow:(id)sender {
    UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"didyouknowview"];
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController pushViewController:viewController animated:YES];
}
-(IBAction)onHome:(id)sender
{
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMenuViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:(sender != nil)];
            break;
        }
    }
}

@end
