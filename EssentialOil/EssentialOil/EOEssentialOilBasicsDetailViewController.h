//
//  EODidYouKnowDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "EOEssentialOilBasicsViewController.h"
@interface EOEssentialOilBasicsDetailViewController : UIViewController
@property EOEssentialOilBasicsViewController* parentVCX;
@property int selectedIndex;
@property AppDelegate* appDelegate;
@end
