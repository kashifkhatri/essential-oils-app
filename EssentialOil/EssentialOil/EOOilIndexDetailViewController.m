//
//  EOOilIndexDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 2/16/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EOPrecautionViewController.h"
#import "EOOilIndexDetailViewController.h"
#import "AppDelegate.h"
#import "DWTagList.h"
#import "EOMenuViewController.h"
#import "EOMyNotesDetailViewController.h"
#import "EOCommonUseDetailViewController.h"

@interface EOOilIndexDetailViewController()
@property (weak, nonatomic) IBOutlet DWTagList *commonUsesList;
@property (weak, nonatomic) IBOutlet UIView *addFavouriteNoteFrame;
@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIButton *addToFavouriteButton;
@property (weak, nonatomic) IBOutlet UIImageView *heartIcon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *countInMyOilsLabel;
@property (weak, nonatomic) IBOutlet UILabel *favouriteLabel;
@property AppDelegate* appDelegate;
@end
@implementation EOOilIndexDetailViewController
@synthesize appDelegate;

- (IBAction)onPlus:(id)sender {
    
    NSInteger offset = ((UIButton*)sender).tag - 1790;
    
    int value = [_countInMyOilsLabel.text intValue];
    value += offset;
    
    if (value < 0) return;
    
    [_countInMyOilsLabel setText:[NSString stringWithFormat:@"%d", value]];
}

-(void)saveCountInMyOils
{
    if ([_countInMyOilsLabel.text integerValue] > 0) {

        [appDelegate.myOils setObject:@[_countInMyOilsLabel.text] forKey:[NSString stringWithFormat:@"%d", _selectedIndex + 1]];
        
        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.myOils forKey:@"_myOils"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    } else if ([_countInMyOilsLabel.text integerValue] == 0) {
        [appDelegate.myOils removeObjectForKey:[NSString stringWithFormat:@"%d", _selectedIndex + 1]];
        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.myOils forKey:@"_myOils"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (scrollView.contentOffset.y <= -30)
    {
        [self saveCountInMyOils];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    

    if ([appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex + 1]])
    {
        [_noteIcon setHidden:NO];
    }
    else
    {
        [_noteIcon setHidden:YES];
    }
}
-(void)viewDidLoad
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if ([appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex + 1]])
    {
        [_noteIcon setHidden:NO];
    }
    else
    {
        [_noteIcon setHidden:YES];
    }
    
    CGSize maximumLabelSize = CGSizeMake(278, FLT_MAX);
    
    
    _aboutLabel.text = [NSString stringWithFormat:@"About:\n%@", appDelegate.oilNames[_selectedIndex][2]];
    CGSize expectedLabelSize = [_aboutLabel.text sizeWithFont:_aboutLabel.font constrainedToSize:maximumLabelSize lineBreakMode:_aboutLabel.lineBreakMode];
    
    NSLog(@"%@",_aboutLabel.text);

    CGRect newFrame = _aboutLabel.frame;

    newFrame.size.height = expectedLabelSize.height;
    _aboutLabel.frame = newFrame;
    
    CGRect rt = _subView.frame;
    
    rt.origin.y = _aboutLabel.frame.origin.y + _aboutLabel.frame.size.height + 15;
    
    _subView.frame = rt;
    NSLog(@"%@", appDelegate.oilNames[_selectedIndex]);
    NSArray* arrCommonUsesID = [appDelegate.oilNames[_selectedIndex][5] componentsSeparatedByString: @","];
    
    NSMutableArray* arrCommonUses = [[NSMutableArray alloc]init];
    
    NSLog(@"%@",appDelegate.commonUses);
    NSLog(@"%@",arrCommonUsesID);
    
    NSMutableArray *arrayId = [[NSMutableArray alloc]init];
    for (int i = 0; i < [arrCommonUsesID count]; i++)
    {
        int s = [arrCommonUsesID[i] intValue];
        NSLog(@"%d",s);
        if (s > 0)
        {
            @try {
                [arrCommonUses addObject:appDelegate.commonUses[s - 1][2]];
                [arrayId addObject:[NSNumber numberWithInt:s - 1]];
            }
            @catch (NSException *exception) {
                NSLog(@"%@",[exception userInfo]);
            }
//            NSLog(@"%@",appDelegate.commonUses[s - 1][2]);
//            [arrCommonUses addObject:appDelegate.commonUses[s - 1][2]];
//            [arrayId addObject:[NSNumber numberWithInt:s - 1]];
//            NSLog(@"-====-");
        }
    }
    
    NSLog(@"%@",arrayId);
    [_commonUsesList setTags:arrCommonUses];
    
    rt = _commonUsesList.frame;
    rt.size.height = _commonUsesList.contentSize.height;
    _commonUsesList.frame = rt;

    rt = _addFavouriteNoteFrame.frame;
    rt.origin.y = _commonUsesList.frame.origin.y + _commonUsesList.frame.size.height + 10;
    _addFavouriteNoteFrame.frame = rt;

    rt = _subView.frame;
    rt.size.height = _addFavouriteNoteFrame.frame.origin.y + _addFavouriteNoteFrame.frame.size.height + 40;
    _subView.frame = rt;

    rt = _mainScrollView.frame;
    rt.size.height = _subView.frame.origin.y
            + _subView.frame.size.height;
    
    _mainScrollView.contentSize = rt.size;
    
    
    if ([appDelegate.oilIndexFavourites objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex]])
    {
        [_addToFavouriteButton setImage:[UIImage imageNamed:@"ES_hearts.png"] forState:UIControlStateNormal];
        [_heartIcon setHidden:NO];
    }
    else {
        [_addToFavouriteButton setImage:[UIImage imageNamed:@"ES_IconFavorites.png"] forState:UIControlStateNormal];
        [_heartIcon setHidden:YES];
    }
    
    [_nameLabel setText:appDelegate.oilNames[_selectedIndex][1]];
    if ([appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex + 1]])
    {
        
        NSString *strCound = [appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex + 1]][0];
        [_countInMyOilsLabel setText:[NSString stringWithFormat:@"%@",strCound]];
    }
    else {
        [_countInMyOilsLabel setText:@"0"];
    }
    
    NSArray* arrays = [_commonUsesList subviews];
    
    for (int i = 0; i < [arrays count]; i++)
    {
        UIView* cView = arrays[i];
        if ([cView isKindOfClass:[DWTagView class]])
        {
            UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onClickedCommonUseItem:)];
            recognizer.numberOfTapsRequired = 1;            
            cView.userInteractionEnabled = YES;
            cView.tag = [arrayId[i] intValue] + 800;
            [cView addGestureRecognizer:recognizer];
            
        }
    }
    
    [_commonUsesList centerAlign];
    
}
- (IBAction)onHowToUse:(id)sender {
    EOPrecautionViewController * viewController = (EOPrecautionViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"precaution"];
    viewController.mode = 2;
    viewController.precautionIDs = [appDelegate.oilNames[_selectedIndex][3] componentsSeparatedByString:@","];
    [self presentViewController:viewController animated:YES completion:nil];
}
- (IBAction)onPrecautions:(id)sender {
    EOPrecautionViewController * viewController = (EOPrecautionViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"precaution"];
    viewController.mode = 1;
    viewController.precautionIDs = [appDelegate.oilNames[_selectedIndex][4] componentsSeparatedByString:@","];
    [self presentViewController:viewController animated:YES completion:nil];
}
- (IBAction)onAddNote:(id)sender {
    EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
    
    viewController.noteID = _selectedIndex + 1;
    viewController.parentVCX = self;
    viewController.stringTitle = _nameLabel.text;
    [self presentViewController:viewController animated:YES completion:nil];
}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMenuViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:(sender != nil)];
            break;
        }
    }
}
- (IBAction)onAddToFavourites:(id)sender {
    
    NSLog(@"%@", appDelegate.oilIndexFavourites);
    if (![appDelegate.oilIndexFavourites objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex]])
    {
        [appDelegate.oilIndexFavourites setObject:@"1" forKey:[NSString stringWithFormat:@"%d", _selectedIndex]];
        [_addToFavouriteButton setImage:[UIImage imageNamed:@"ES_hearts.png"] forState:UIControlStateNormal];
        [_heartIcon setHidden:NO];
        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.oilIndexFavourites forKey:@"_oilIndexFavourites"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        _favouriteLabel.text = @"Remove Favorite";
    }
    else
    {

        [appDelegate.oilIndexFavourites removeObjectForKey:[NSString stringWithFormat:@"%d", _selectedIndex]];
        [_addToFavouriteButton setImage:[UIImage imageNamed:@"ES_IconFavorites.png"] forState:UIControlStateNormal];
        [_heartIcon setHidden:YES];
        
        [[NSUserDefaults standardUserDefaults]setObject:appDelegate.oilIndexFavourites forKey:@"_oilIndexFavourites"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        _favouriteLabel.text = @"Add to Favorites";
    }
}
-(void)onClickedCommonUseItem:(UITapGestureRecognizer*)recog
{
    UILabel* sender = (UILabel*)(recog.view);
    EOCommonUseDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"commonusedetailview"];
    

    NSLog(@"%ld",(long)[((UILabel*)sender) tag]);
    NSLog(@"%@",appDelegate.oilNames);

    int rowIndex = ((UILabel*)sender).tag - 799;
    
    
    viewController.selectedIndex = rowIndex - 1;
    int oilCount = [appDelegate.oilNames count];
    
    
    NSString* temp;
    NSArray* tempArr;
    NSMutableArray* usedArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < oilCount; i++)
    {
        temp = appDelegate.oilNames[i][5];
        
        tempArr = [temp componentsSeparatedByString:@","];
        int flag = 0;
        for (NSString* c in tempArr)
        {
            int p = [c intValue];
            if (p < 1) continue;
            
            if(rowIndex == p)
            {
                flag = 1; break;
            }
        }
        
        if (flag)
        {
            NSLog(@" ==== %d ===== ", i);
            [usedArr addObject:appDelegate.oilNames[i][1]];
            NSLog(@"%@",appDelegate.oilNames[i][1]);
        }
    }
    
    viewController.arrUsedOils = [[NSArray alloc]initWithArray:usedArr];
    [self presentViewController:viewController animated:YES completion:nil];
}
@end
