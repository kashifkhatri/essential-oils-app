//
//  EOMyOilsAddContactViewController.m
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyOilsAddContactViewController.h"
#import "EOMyOilsMainSharingViewController.h"
#import "AppDelegate.h"
@interface EOMyOilsAddContactViewController()
@property AppDelegate* appDelegate;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end
@implementation EOMyOilsAddContactViewController
-(void)viewDidLoad
{
    [super viewDidLoad];
    _appDelegate = [[UIApplication sharedApplication]delegate];
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_appDelegate.myContacts count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_appDelegate.myContacts[indexPath.row] count] == 3 && [@"1" isEqualToString:_appDelegate.myContacts[indexPath.row]])
        return 0;
    return 35;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = _appDelegate.myContacts[indexPath.row][1][0];
    cell.delegate = self;
    cell.indexPath = indexPath;
    
    cell.buttonCount = 0;
    
//    NSArray* imageButton = @[@"ES_SettingIconBack.png", @"ES_SettingIconFavorite.png", @"ES_SettingIconNotepad.png",
//                             @"ES_SettingIconTrash.png"];
//    
//    [cell.scrollViewButtonView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1]];
//    
//    if (indexPath.row == 3)
//    {
//        int s = 0;
//        s = 1;
//    }
//    for (int i = 0; i < 4; i++)
//    {
//        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        moreButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
//        int height = 49 - 20;
//        if (height < 0) height=0;
//        moreButton.frame = CGRectMake(49 * i + 10, 10, 49 - 20, height);
//        [moreButton setImage:[UIImage imageNamed:imageButton[i]] forState:UIControlStateNormal];
//        [moreButton setTag:i + indexPath.row * 5];
//        [moreButton addTarget:self action:@selector(userPressedMoreButton:) forControlEvents:UIControlEventTouchUpInside];
//        [cell.scrollViewButtonView addSubview:moreButton];
//    }
    
    
    
    
    return cell;
}

-(void)onSelectCell:(TLSwipeForOptionsCell*)cell
{
    EOMyOilsMainSharingViewController* viewController = (EOMyOilsMainSharingViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsmainsharing"];
    
    viewController.selectedIndex = _selectedIndex;
    viewController.selectedContact = cell.indexPath.row;
    viewController.parentVCX = _parentVCX;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
