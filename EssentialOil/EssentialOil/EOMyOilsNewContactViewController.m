//
//  EOMyOilsNewContactViewController.m
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyOilsNewContactViewController.h"
#import "EOMyContactsDetailViewController.h"
#import "EOMyContactsViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "UIPlaceHolderTextView.h"
#import "EOMyOilsMainSharingViewController.h"

@interface EOMyOilsNewContactViewController ()
@property (weak, nonatomic) IBOutlet UITextField *contactName;
@property (weak, nonatomic) IBOutlet UILabel *contactType;
@property (weak, nonatomic) IBOutlet UITextField *contactNo;
@property (weak, nonatomic) IBOutlet UITextField *contactEmail;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *address;
@property (weak, nonatomic) IBOutlet UITextField *web;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *notes;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property AppDelegate* appDelegate;
@property NSArray* types;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property int curTypeIndex;
@end

@implementation EOMyOilsNewContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _types = @[@"Lead", @"Friend", @"Family", @"Resource", @"Colleague", @"Other"];
    
    _selectedIndex = -1;
    // Do any additional setup after loading the view.
    _appDelegate = [[UIApplication sharedApplication]delegate];
    
    if (_selectedIndex >= 0 && _selectedIndex < [_appDelegate.myContacts count])
    {
        [_contactName setText:_appDelegate.myContacts[_selectedIndex][1][0]];
        _curTypeIndex = [_appDelegate.myContacts[_selectedIndex][1][1] intValue];
        [_contactType setText:_types[_curTypeIndex]];
        
        [_contactNo setText:_appDelegate.myContacts[_selectedIndex][1][2]];
        
        [_contactEmail setText:_appDelegate.myContacts[_selectedIndex][1][3]];
        [_address setText:_appDelegate.myContacts[_selectedIndex][1][4]];
        [_web setText:_appDelegate.myContacts[_selectedIndex][1][5]];
        [_notes setText:_appDelegate.myContacts[_selectedIndex][1][6]];
        [_addButton setImage:[UIImage imageNamed:@"ES_ButtonEdit.png"] forState:UIControlStateNormal];
    }
    else {
        _curTypeIndex = 0;
        [_contactType setText:_types[_curTypeIndex]];
    }
    self.address.layer.borderWidth = 1.0f;
    self.address.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1] CGColor];
    self.address.layer.cornerRadius = 5;
    
    self.notes.layer.borderWidth = 1.0f;
    self.notes.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1] CGColor];
    self.notes.layer.cornerRadius = 5;
    
    CGSize size = self.mainScrollView.frame.size;
    size.height += 240;
    self.mainScrollView.contentSize = size;
    
    self.address.placeholder = @"Address";
    self.notes.placeholder = @"Notes";
    
    UITapGestureRecognizer* rec = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onHideKeyboard:)];
    rec.numberOfTapsRequired = 1;
    [self.mainScrollView addGestureRecognizer:rec];
}
-(void)onHideKeyboard:(id)sender
{
    for (UIView* view in _mainScrollView.subviews)
        [view resignFirstResponder];
}
- (IBAction)onPrev:(id)sender {
    _curTypeIndex = (_curTypeIndex + 5) % 6;
    [_contactType setText:_types[_curTypeIndex]];
}

- (IBAction)onNext:(id)sender {
    _curTypeIndex = (_curTypeIndex + 1) % 6;
    [_contactType setText:_types[_curTypeIndex]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onHome:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onNewContact:(id)sender {
    
    NSString* msg = @"Successfully Updated";
    NSString *strContactName = _contactName.text;
    strContactName = [strContactName stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([_contactName.text length] < 1) {
        msg = @"Enter Contact name";
        
    } else if([strContactName length] < 1)
    {
        msg = @"Enter Contact name";
    }
    else
    {

        NSString* msg = @"Successfully Updated";
        if (_selectedIndex == -1)
        {
            _selectedIndex = [_appDelegate.myContacts count];
            [_appDelegate.myContacts addObject:@""];
            msg = @"Successfully Added";
        }
        
        NSMutableArray* arr = [[NSMutableArray alloc]init];
        [arr addObject:[NSString stringWithFormat:@"%d", _selectedIndex]];
        [arr addObject:@[_contactName.text, [NSNumber numberWithInt:_curTypeIndex], _contactNo.text, _contactEmail.text, _address.text, _web.text, _notes.text]];
        [_appDelegate.myContacts replaceObjectAtIndex:_selectedIndex withObject:arr];
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myContacts forKey:@"_myContacts"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
        EOMyOilsMainSharingViewController* viewController = (EOMyOilsMainSharingViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsmainsharing"];
        
        viewController.selectedIndex = _selectedIndexString;
        viewController.selectedContact = _selectedIndex;
    
        [self.navigationController pushViewController:viewController animated:YES];
    }
    [SVProgressHUD showSuccessWithStatus:msg];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        _appDelegate.myContacts[_selectedIndex] = @[_appDelegate.myContacts[_selectedIndex][0], _appDelegate.myContacts[_selectedIndex][1], @"1"];
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myContacts forKey:@"_myContacts"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [SVProgressHUD showWithStatus:@"Successfully Deleted"];
        [self performSelector:@selector(onDismiss) withObject:nil afterDelay:2];
        
    }
}

-(void)onDismiss
{
    [SVProgressHUD dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
