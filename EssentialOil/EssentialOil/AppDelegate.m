//
//  AppDelegate.m
//  EssentialOil
//
//  Created by Steve on 1/4/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "AppDelegate.h"
#import "CSVParser.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(NSArray*)parseCSVText:(NSString*)fileName
{
    NSString* path = [ [ NSBundle mainBundle ] pathForResource : fileName ofType : @"csv" ] ;
    
    NSError* error;
    NSString* content = [ NSString stringWithContentsOfFile : path encoding : NSUTF8StringEncoding error : &error ] ;
    
    if (error) return @[];
    
    CSVParser* parser = [ [ CSVParser alloc ] initWithString : content separator : @"," ] ;
    NSArray* oil_names = [ parser Parse ] ;

    return oil_names;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
  
    NSUserDefaults* userData = [NSUserDefaults standardUserDefaults];
    
    NSString *ver = [userData objectForKey:@"version"];
    float verNum = (ver == nil ? 0 : [ver floatValue]);
    
    int flag = 0;
//----------------------------------------------------------------------
    if (!(_oilNames = [userData objectForKey:@"oilNames"]) || verNum < CURRENT_VERSION)
    {
        _oilNames = [[NSMutableArray alloc]initWithArray:[self parseCSVText:@"oil_names"]];
        [userData setObject:_oilNames forKey:@"oilNames"];
        flag = 1;
    }
    else _oilNames = [[NSMutableArray alloc]initWithArray:_oilNames];
//----------------------------------------------------------------------
    if (!(_generalInformation = [userData objectForKey:@"generalInformation"]) || verNum < CURRENT_VERSION)
    {
        _generalInformation = [[NSMutableArray alloc]initWithArray:[self parseCSVText:@"GeneralInformation"]];
        [userData setObject:_generalInformation forKey:@"generalInformation"];
        flag = 1;
    }
    else _generalInformation = [[NSMutableArray alloc]initWithArray:_generalInformation];
//----------------------------------------------------------------------
    if (!(_commonUses = [userData objectForKey:@"commonUses"]) || verNum < CURRENT_VERSION)
    {
        _commonUses = [[NSMutableArray alloc]initWithArray:[self parseCSVText:@"commonuses"]];
        [userData setObject:_commonUses forKey:@"commonUses"];
        flag = 1;
    }
    else _commonUses = [[NSMutableArray alloc]initWithArray:_commonUses];
//----------------------------------------------------------------------
    if (!(_tips = [userData objectForKey:@"tips"]) || verNum < CURRENT_VERSION)
    {
        _tips = [[NSMutableArray alloc]initWithArray:[self parseCSVText:@"tips"]];
        [userData setObject:_tips forKey:@"tips"];
        flag = 1;
    }
    else _tips = [[NSMutableArray alloc]initWithArray:_tips];
//----------------------------------------------------------------------
    if (!(_preCaution = [userData objectForKey:@"preCaution"]) || verNum < CURRENT_VERSION)
    {
        _preCaution = [[NSMutableArray alloc]initWithArray:[self parseCSVText:@"precaution"]];
        [userData setObject:_preCaution forKey:@"preCaution"];
        flag = 1;
    }
    else _preCaution = [[NSMutableArray alloc]initWithArray:_preCaution];
//----------------------------------------------------------------------
    if (!(_howToUse = [userData objectForKey:@"howToUse"]) || verNum < CURRENT_VERSION)
    {
        _howToUse = [[NSMutableArray alloc]initWithArray:[self parseCSVText:@"howtouse"]];
        [userData setObject:_howToUse forKey:@"howToUse"];
        flag = 1;
    }
    else _howToUse = [[NSMutableArray alloc]initWithArray:_howToUse];
//----------------------------------------------------------------------
    if (!(_oilIndexFavourites = [userData objectForKey:@"_oilIndexFavourites"]) || verNum < CURRENT_VERSION)
    {
        _oilIndexFavourites = [[NSMutableDictionary alloc]init];
        [userData setObject:_oilIndexFavourites forKey:@"_oilIndexFavourites"] ;
        flag = 1;
    }
    else _oilIndexFavourites = [[NSMutableDictionary alloc]initWithDictionary:_oilIndexFavourites];
    
    
    
//----------------------------------------------------------------------
    

    if (!(_blendsFavourites = [userData objectForKey:@"_blendsFavourites"]) || verNum < CURRENT_VERSION)
    {
        _blendsFavourites = [[NSMutableDictionary alloc]init];
        [userData setObject:_blendsFavourites forKey:@"_blendsFavourites"] ;
        flag = 1;
    }
    else _blendsFavourites = [[NSMutableDictionary alloc]initWithDictionary:_blendsFavourites];
    
    
//----------------------------------------------------------------------
   
    

    if (!(_commonUsesFavourites = [userData objectForKey:@"_commonUsesFavourites"]) || verNum < CURRENT_VERSION)
    {
        _commonUsesFavourites = [[NSMutableDictionary alloc]init];
        [userData setObject:_commonUsesFavourites forKey:@"_commonUsesFavourites"] ;
        flag = 1;
    }
    else _commonUsesFavourites = [[NSMutableDictionary alloc]initWithDictionary:_commonUsesFavourites];
   
///-----Oil Notes-----------------------------------
    
    if (!(_oilNotes = [userData objectForKey:@"_oilNotes"]) || verNum < CURRENT_VERSION)
    {
        _oilNotes = [[NSMutableDictionary alloc]init];
        [userData setObject:_oilNotes forKey:@"_oilNotes"] ;
        flag = 1;
    }
    else _oilNotes = [[NSMutableDictionary alloc]initWithDictionary:_oilNotes];
   
//----------------------------------------------------------------------
    
    if (!(_myOils = [userData objectForKey:@"_myOils"]) || verNum < CURRENT_VERSION)
    {
        _myOils = [[NSMutableDictionary alloc]init];
        [userData setObject:_myOils forKey:@"_myOils"] ;
        flag = 1;
    }
    else _myOils = [[NSMutableDictionary alloc]initWithDictionary:_myOils];

//----------------------------------------------------------------------
    
    if (!(_myBlends = [userData objectForKey:@"_myBlends"]) || verNum < CURRENT_VERSION)
    {
        _myBlends = [[NSMutableArray alloc]init];
        [userData setObject:_myBlends forKey:@"_myBlends"] ;
        flag = 1;
    }
    else _myBlends = [[NSMutableArray alloc]initWithArray:_myBlends];

//----------------------------------------------------------------------
    
    if (!(_myContacts = [userData objectForKey:@"_myContacts"]) || verNum < CURRENT_VERSION)
    {
        _myContacts = [[NSMutableArray alloc]init];
        [userData setObject:_myContacts forKey:@"_myContacts"] ;
        flag = 1;
    }
    else _myContacts = [[NSMutableArray alloc]initWithArray:_myContacts];
    
//----------------------------------------------------------------------
    
    if (!(_contactFavourites = [userData objectForKey:@"_contactFavourites"]) || verNum < CURRENT_VERSION)
    {
        _contactFavourites = [[NSMutableDictionary alloc]init];
        [userData setObject:_contactFavourites forKey:@"_contactFavourites"] ;
        flag = 1;
    }
    else _contactFavourites = [[NSMutableDictionary alloc]initWithDictionary:_contactFavourites];
    
    //----------------------------------------------------------------------
    
    if (!(_noteFavourites = [userData objectForKey:@"_noteFavourites"]) || verNum < CURRENT_VERSION)
    {
        _noteFavourites = [[NSMutableDictionary alloc]init];
        [userData setObject:_noteFavourites forKey:@"_noteFavourites"] ;
        flag = 1;
    }
    else _noteFavourites = [[NSMutableDictionary alloc]initWithDictionary:_noteFavourites];
    
    //----------------------------------------------------------------------
    
    if (!(_myOilsShare = [userData objectForKey:@"_myOilsShare"]) || verNum < CURRENT_VERSION)
    {
        _myOilsShare = [[NSMutableDictionary alloc]init];
        [userData setObject:_myOilsShare forKey:@"_myOilsShare"] ;
        flag = 1;
    }
    else _myOilsShare = [[NSMutableDictionary alloc]initWithDictionary:_myOilsShare];
    
    
    
    if (flag)
        [userData synchronize];
    
    
    return YES;
}
-(void)saveData
{
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) saveOilData:(NSMutableDictionary *) dictionary
{
    _myOils = [[NSMutableDictionary alloc]initWithDictionary:dictionary];
}

@end
