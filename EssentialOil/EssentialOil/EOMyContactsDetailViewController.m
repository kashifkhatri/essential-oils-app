//
//  EODidYouKnowDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyContactsDetailViewController.h"
#import "EOMyContactsViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "UIPlaceHolderTextView.h"
#import "EOMyContactsViewController.h"

@interface EOMyContactsDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *contactName;
@property (weak, nonatomic) IBOutlet UILabel *contactType;
@property (weak, nonatomic) IBOutlet UITextField *contactNo;
@property (weak, nonatomic) IBOutlet UITextField *contactEmail;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *address;
@property (weak, nonatomic) IBOutlet UITextField *web;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *notes;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property AppDelegate* appDelegate;
@property NSArray* types;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property int curTypeIndex;
@end

@implementation EOMyContactsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _types = @[@"Lead", @"Friend", @"Family", @"Resource", @"Colleague", @"Other"];
    // Do any additional setup after loading the view.
    _appDelegate = [[UIApplication sharedApplication]delegate];
    
    if (_selectedIndex >= 0 && _selectedIndex < [_appDelegate.myContacts count])
    {
        [_contactName setText:_appDelegate.myContacts[_selectedIndex][1][0]];
        _curTypeIndex = [_appDelegate.myContacts[_selectedIndex][1][1] intValue];
        [_contactType setText:_types[_curTypeIndex]];

        [_contactNo setText:_appDelegate.myContacts[_selectedIndex][1][2]];

        [_contactEmail setText:_appDelegate.myContacts[_selectedIndex][1][3]];
        [_address setText:_appDelegate.myContacts[_selectedIndex][1][4]];
        [_web setText:_appDelegate.myContacts[_selectedIndex][1][5]];
        [_notes setText:_appDelegate.myContacts[_selectedIndex][1][6]];
        [_addButton setImage:[UIImage imageNamed:@"ES_ButtonEdit.png"] forState:UIControlStateNormal];
    }
    else {
        _curTypeIndex = 0;
        [_contactType setText:_types[_curTypeIndex]];
    }
    self.address.layer.borderWidth = 1.0f;
    self.address.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1] CGColor];
    self.address.layer.cornerRadius = 5;
    
    self.notes.layer.borderWidth = 1.0f;
    self.notes.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1] CGColor];
    self.notes.layer.cornerRadius = 5;
    
    CGSize size = self.mainScrollView.frame.size;
    size.height += 320;
    self.mainScrollView.contentSize = size;

    self.address.placeholder = @"Address";
    self.notes.placeholder = @"Notes";
    
    UITapGestureRecognizer* rec = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onHideKeyboard:)];
    rec.numberOfTapsRequired = 1;
    [self.mainScrollView addGestureRecognizer:rec];
}
-(void)onHideKeyboard:(id)sender
{
    for (UIView* view in _mainScrollView.subviews)
         [view resignFirstResponder];
}
- (IBAction)onPrev:(id)sender {
    _curTypeIndex = (_curTypeIndex + 5) % 6;
    [_contactType setText:_types[_curTypeIndex]];
}

- (IBAction)onNext:(id)sender {
    _curTypeIndex = (_curTypeIndex + 1) % 6;
    [_contactType setText:_types[_curTypeIndex]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onHome:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_parentVCX isKindOfClass:[EOMyContactsViewController class]])
        {
            [(EOMyContactsViewController*)_parentVCX onHome:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Nav_Home" object:nil];
        }
    }];
}
- (IBAction)onBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onNewContact:(id)sender {
    
    NSString* msg = @"Successfully Updated";
    NSString *strContactName = _contactName.text;
    strContactName = [strContactName stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([_contactName.text length] < 1) {
        msg = @"Enter Contact name";
        
    } else if([strContactName length] < 1)
    {
        msg = @"Enter Contact name";
    }
    else
    {
        msg = @"Successfully Updated";
        if (_selectedIndex == -1)
        {
            _selectedIndex = [_appDelegate.myContacts count];
            [_appDelegate.myContacts addObject:@""];
            msg = @"Successfully Added";
        }
        
        NSMutableArray* arr = [[NSMutableArray alloc]init];
        [arr addObject:[NSString stringWithFormat:@"%d", _selectedIndex]];
        [arr addObject:@[_contactName.text, [NSNumber numberWithInt:_curTypeIndex], _contactNo.text, _contactEmail.text, _address.text, _web.text, _notes.text]];
        [_appDelegate.myContacts replaceObjectAtIndex:_selectedIndex withObject:arr];
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myContacts forKey:@"_myContacts"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [SVProgressHUD showSuccessWithStatus:msg];
    

}
- (IBAction)onShare:(id)sender {
    if (_selectedIndex < 0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"This contact is not saved yet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }
    NSArray* valueArr= @[_contactName.text, _types[_curTypeIndex], _contactNo.text, _contactEmail.text, _address.text, _web.text, _notes.text];
    NSArray* keyArr = @[@"Contact Name", @"Contact Type", @"Phone No.", @"E-mail Address", @"Address", @"Website", @"Notes"];
    
    NSString *textToShare = @"";
    for (int i = 0; i <= 6; i++)
        textToShare = [NSString stringWithFormat:@"%@%@:%@\n", textToShare, keyArr[i], valueArr[i]];
    
    
    
    //NSURL *myWebsite = [NSURL URLWithString:@"http://www.apple.com/"];
    
    NSArray *objectsToShare = @[textToShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[
                                   UIActivityTypePrint,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypeCopyToPasteboard,
                                   UIActivityTypePostToFacebook,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToTwitter,
                                   UIActivityTypePostToTencentWeibo,
                                   UIActivityTypePostToWeibo
                                   ];

    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {

        NSLog(@"%@",_appDelegate.myContacts);
        NSLog(@"%@",_appDelegate.myOilsShare);
        
        NSArray *arrayKeys = [_appDelegate.myOilsShare allKeys];
        
        for (int i =0 ; i < [arrayKeys count]; i++) {
            NSString *keyId = [arrayKeys objectAtIndex:i];
            
            NSMutableDictionary *_dataDict = [[NSMutableDictionary alloc]initWithDictionary:[_appDelegate.myOilsShare objectForKey:keyId]];
            
            NSArray *allKeys = [_dataDict allKeys];
            
            if ([allKeys count] > 0) {
                NSLog(@"%ld",[[allKeys objectAtIndex:0] integerValue]);
                NSLog(@"%ld",[[[_appDelegate.myContacts objectAtIndex:_selectedIndex] objectAtIndex:0] integerValue]);
                
                NSLog(@"%@",[allKeys objectAtIndex:_selectedIndex]);
                
                [[_appDelegate.myOilsShare objectForKey:keyId] removeObjectForKey:[NSString stringWithFormat:@"%d",_selectedIndex]];
            }            
        }
        
        [_appDelegate.myContacts removeObjectAtIndex:_selectedIndex];
        
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOilsShare forKey:@"_myOilsShare"];
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myContacts forKey:@"_myContacts"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [SVProgressHUD showSuccessWithStatus:@"Successfully Removed!"];
        
        NSLog(@"%@",_appDelegate.myOilsShare);
        NSLog(@"%@",_appDelegate.myContacts);
        
        //_appDelegate.myContacts[_selectedIndex] = @[_appDelegate.myContacts[_selectedIndex][0], _appDelegate.myContacts[_selectedIndex][1], @"1"];
//        [_appDelegate.myContacts removeObjectAtIndex:_selectedIndex];
//        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myContacts forKey:@"_myContacts"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//        [SVProgressHUD showWithStatus:@"Successfully Deleted"];
        [self performSelector:@selector(onDismiss) withObject:nil afterDelay:1];
        
    }
}
-(void)onDismiss
{
    [SVProgressHUD dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onDeleteContact:(id)sender {
    if (_selectedIndex == -1) return;
    [[[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure to delete this contact?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil]show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
