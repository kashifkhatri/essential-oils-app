//
//  EOWelcomeView.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOWelcomeView.h"
#import "CSVParser.h"

@interface EOWelcomeView ()
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end


@implementation EOWelcomeView

- (void)viewDidLoad {
    [super viewDidLoad];
    

            
    // Do any additional setup after loading the view.
    CGSize size = [[UIScreen mainScreen]bounds].size;
    
    if ([[[UIDevice currentDevice]systemVersion] floatValue] < 7.0)
        size.height -= 0;
    [_mainScrollView setContentSize:CGSizeMake(size.width * 7, size.height -20)];
    [_mainScrollView setFrame:CGRectMake(0, 0, size.width, size.height)];
    for (int i = 1; i <= 7; i++)
    {
        UIView* view = [self.view viewWithTag:i];
        [view setFrame:CGRectMake(size.width * (i - 1), 0, size.width, size.height)];
        [_mainScrollView addSubview:view];
        
        
    }

    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *file = [mainBundle pathForResource: @"oil_names" ofType: @"csv"];
    
    UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onNextScreen)];
    recognizer.numberOfTapsRequired = 1;
    [[self.view viewWithTag:7] addGestureRecognizer:recognizer];
}
-(void)onNextScreen
{
    UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuview"];
    
    [self.navigationController pushViewController:viewController animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onPageControlValueChanged:(id)sender {
    CGSize size = [[UIScreen mainScreen]bounds].size;
    [self.mainScrollView setContentOffset:CGPointMake(_pageControl.currentPage * size.width, 0)];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.mainScrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = self.mainScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
