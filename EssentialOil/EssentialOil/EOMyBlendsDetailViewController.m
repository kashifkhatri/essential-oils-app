//
//  EOMyBlendsDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 2/11/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyBlendsDetailViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
@interface EOMyBlendsDetailViewController()

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property NSMutableArray* oilNames;
@property NSMutableArray* dropsCount;
@property (weak, nonatomic) IBOutlet UITextField* blendName;
@property (weak, nonatomic) IBOutlet UIView *buttonFrame;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property int currentEditingIndex, indexRowToDelete;
@property UITableViewCell* currentEditCell;
@property NSMutableArray* wordsArr, *flagArr;
@end

@implementation EOMyBlendsDetailViewController
-(void)viewDidLoad
{
    
    NSLog(@"%@",_appDelegate.oilNames);
    NSLog(@"%@",_appDelegate.myOils);
    NSLog(@"%@",_appDelegate.myBlends);
    
    [super viewDidLoad];
    _appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    if (_selectedIndex == -1)
    {
        _dropsCount = [[NSMutableArray alloc]init];
        [_dropsCount addObject:[NSNumber numberWithInt:1]];
        _oilNames = [[NSMutableArray alloc]init];
        [_oilNames addObject:@""];
    }
    else
    {
        _blendName.text = _appDelegate.myBlends[_selectedIndex][1];
        _dropsCount = [[NSMutableArray alloc]init];
        _oilNames = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < [_appDelegate.myBlends[_selectedIndex][2] count]; i++)
        {
            [_oilNames addObject:_appDelegate.myBlends[_selectedIndex][2][i][0]];
            [_dropsCount addObject:_appDelegate.myBlends[_selectedIndex][2][i][1]];
        }
    }
    _blendName.layer.borderWidth = 0;
    [self resizeTable];
    
    _wordsArr = [[NSMutableArray alloc]init];
    _flagArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < [_appDelegate.oilNames count]; i++)
    {
        [_wordsArr addObject:_appDelegate.oilNames[i][1]];
        
        [_flagArr addObject:@"0"];
    }
    
    UITapGestureRecognizer* recog = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelRecommendOilname)];
    
    recog.numberOfTapsRequired = 1;
    [recog setCancelsTouchesInView:NO];
    //[_mainScrollView addGestureRecognizer:recog];
    UITapGestureRecognizer* recog1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelAllTextFields)];
    
    recog1.numberOfTapsRequired = 1;
    
    //[self.view addGestureRecognizer:recog1];
}
-(void)cancelAllTextFields
{
    [self cancelRecommendOilname];
    [_blendName resignFirstResponder];
    
}
-(void)resizeTable
{
    CGRect rt = [_tableView frame];
    rt.size.height = 120 * [_oilNames count];
    [_tableView setFrame:rt];
    
    CGRect rt1 = [_buttonFrame frame];
    
    rt1.origin.y = rt.origin.y + rt.size.height + 20;
    [_buttonFrame setFrame:rt1];
    [_mainScrollView setContentSize:CGSizeMake(rt1.size.width,rt1.size.height + rt1.origin.y + 130)];
    
}
- (IBAction)onBack:(id)sender {
    if (_selectedIndex > -1)
    {
        NSMutableArray* arr = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < [_dropsCount count]; i++)
        {
            [arr addObject:@[_oilNames[i], _dropsCount[i]]];
        }
        
        [_appDelegate.myBlends replaceObjectAtIndex:_selectedIndex withObject:@[[NSString stringWithFormat:@"%d",_selectedIndex], _blendName.text, arr]];
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myBlends forKey:@"_myBlends"];
        [[NSUserDefaults standardUserDefaults]synchronize];

    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onPlus:(id)sender {
    [self cancelAllMenu];
    UIView* cellView = (UIView*)[((UIButton*)sender) superview];
    [_blendName resignFirstResponder];
    UITableViewCell* tableViewCell;
    
    while (![cellView isKindOfClass:[UITableViewCell class]])
    {
        cellView = [cellView superview];
    }
    
    tableViewCell = (UITableViewCell*)cellView;
    
    NSIndexPath *indexPath = [(UITableView *)_tableView indexPathForCell: tableViewCell];
    
    int index = indexPath.row;
    
    _dropsCount[index] = [NSNumber numberWithInt:[_dropsCount[index] integerValue] + 1];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    [self cancelRecommendOilname];
    
}
- (IBAction)onMinus:(id)sender {
    [self cancelAllMenu];
    UIView* cellView = (UIView*)[((UIButton*)sender) superview];
    
    UITableViewCell* tableViewCell;
    
    while (![cellView isKindOfClass:[UITableViewCell class]])
    {
        cellView = [cellView superview];
    }
        [_blendName resignFirstResponder];
    tableViewCell = (UITableViewCell*)cellView;
    
    NSIndexPath *indexPath = [(UITableView *)_tableView indexPathForCell: tableViewCell];
    
    int index = indexPath.row;
    
    _dropsCount[index] = [NSNumber numberWithInt:MAX([_dropsCount[index] integerValue] - 1, 1)];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    [self cancelRecommendOilname];
    
}
- (IBAction)onAddOil:(id)sender {
    [_dropsCount addObject:[NSNumber numberWithInt:1]];
    [_oilNames addObject:@""];
    [self.tableView reloadData];
    [self resizeTable];
        [_blendName resignFirstResponder];
    [self cancelRecommendOilname];
}
-(void)deleteOil
{
    [_blendName resignFirstResponder];
    [self cancelRecommendOilname];
    [_dropsCount removeObjectAtIndex:_indexRowToDelete];
    [_oilNames removeObjectAtIndex:_indexRowToDelete];
    [self resizeTable];
    [self.tableView reloadData];
}
- (IBAction)onDeleteOil:(id)sender {
    UIView* parentCellView = ((UIButton*)sender);
    while (![parentCellView isKindOfClass:[UITableViewCell class]])
        parentCellView = parentCellView.superview;
    NSIndexPath* indexPath = [_tableView indexPathForCell:(UITableViewCell *)parentCellView];
    _indexRowToDelete = indexPath.row;
    
        [_blendName resignFirstResponder];
        [self cancelRecommendOilname];
    UIAlertView* alertview = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure to delete this selected oil?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertview.tag = 100;
    [alertview show];
    

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 8934)
        return [_wordsArr count];
    return [_oilNames count];
}
-(void)cancelAllMenu
{
    NSArray* array = [_tableView visibleCells];
    
    for (UITableViewCell* cell in array)
    {
        [((UIScrollView*)[cell viewWithTag:970]) setContentOffset:CGPointMake(45, 0)  animated:NO];
    }
}
- (IBAction)onOilNameEditingBegin:(id)sender {
    
    [self cancelAllMenu];
    UIView* cellView = (UIView*)[((UITextField*)sender) superview];
    if (_currentEditCell)
    {
        [[_currentEditCell viewWithTag:8934] setHidden:YES];
    }
    UITableViewCell* tableViewCell;
    
    while (![cellView isKindOfClass:[UITableViewCell class]])
    {
        cellView = [cellView superview];
    }
    
    tableViewCell = (UITableViewCell*)cellView;
    
    NSIndexPath *indexPath = [(UITableView *)_tableView indexPathForCell: tableViewCell];
    _currentEditCell = tableViewCell;
    
    int index = indexPath.row;
    
    
    //NSLog(@"%@", NSStringFromCGRect());
//    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Oil Name" message:@"Enter new oil name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//    av.alertViewStyle = UIAlertViewStylePlainTextInput;
//    UITextField* alertTextField = [av textFieldAtIndex:0];
//    alertTextField.delegate = self;
//    
    _currentEditingIndex = index;
//   
//
//
//    [av show];
//    [alertTextField becomeFirstResponder];
    
    UIWindow* mainWindow = [(AppDelegate*)[[UIApplication sharedApplication]delegate] window];
    
    CGPoint ptInScreen = [mainWindow convertPoint:cellView.frame.origin fromWindow:nil];
    ((UITableView*)([[sender superview] viewWithTag:8934])).hidden = NO;
    int bottomY = [UIScreen mainScreen].bounds.size.height - 253 - 100;
    
//    if (ptInScreen.y < bottomY) return;
//    else [UIView animateWithDuration:0.1 animations:^{
//        CGRect rt= self.view.frame;
//        rt.origin.y = bottomY - ptInScreen.y;
//        [self.view setFrame:rt];
//    }];
    [_mainScrollView setContentOffset:CGPointMake(0, _currentEditCell.frame.origin.y) animated:YES];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag ==8934)
    {
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"recommendcell"];
        if (cell == nil) {
            
            /*
             *   Actually create a new cell (with an identifier so that it can be dequeued).
             */
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"recommendcell"] ;
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            
        }
        cell.textLabel.text = _wordsArr[indexPath.row];
        return cell;
    }
    
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"blendCell" forIndexPath:indexPath];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    UIScrollView* cellScrollView = ((UIScrollView*)[cell viewWithTag:970]);
    [cellScrollView setContentSize:CGSizeMake(365, 120)];
    [cellScrollView setContentOffset:CGPointMake(45, 0) animated:NO];
    cellScrollView.delegate = self;
    
    UITapGestureRecognizer* recog = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapScrollViewCell:)];
    recog.delegate = self;
    recog.numberOfTapsRequired = 1;
//    [recog setCancelsTouchesInView:NO];
    //[cellScrollView addGestureRecognizer:recog];
    
    
    [((UILabel*)[cell viewWithTag:2]) setText:[NSString stringWithFormat:@"%02ld", (long)[_dropsCount[indexPath.row] integerValue]]];
    
    [((UITextField*)([cell viewWithTag:3])) setText:_oilNames[indexPath.row]];
    
    UITableView* subTableView = ((UITableView*)[cellScrollView viewWithTag:8934]);
    [subTableView setDelegate:self];
    [subTableView setDataSource:self];
    subTableView.layer.borderWidth = 1;
    subTableView.layer.borderColor = [[UIColor colorWithWhite:0.7 alpha:1] CGColor];
    subTableView.layer.cornerRadius = 3;
    subTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    subTableView.hidden= YES;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 8934)
    {
        if ([_flagArr[indexPath.row] intValue] == 0)
            return 0;
        return 20;
    }
    return 120;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self cancelAllMenu];
    if (textField == _blendName) {
        [textField resignFirstResponder];
        return YES;
    }
    
    CGRect rt = self.view.frame;
    rt.origin.y = 0;
    [self.view setFrame:rt];
    
    [textField resignFirstResponder];
    
    _oilNames[_currentEditingIndex] = textField.text;
    return YES;

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self cancelAllMenu];
    if (textField == _blendName) {
        [textField resignFirstResponder];
        return YES;
    }
    CGRect rt = self.view.frame;
    rt.origin.y = 0;
    [self.view setFrame:rt];
    
    [textField resignFirstResponder];
    
    _oilNames[_currentEditingIndex] = textField.text;
    return YES;
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) // delete oil
    {
            if (buttonIndex == 1)
            {
                [self deleteOil];
            }
    }
    else if (alertView.tag == 107) // delete blend
    {
        if (buttonIndex == 1)
        {

            if (_selectedIndex < 0) {
                
            } else {
                NSArray* a = _appDelegate.myBlends[_selectedIndex];
                
                _appDelegate.myBlends[_selectedIndex] = @[a[0], a[1], a[2], @"1"];
                
                [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myBlends forKey:@"_myBlends"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [SVProgressHUD showSuccessWithStatus:@"Successfully Removed!"];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
    else if (alertView.tag == 101) // add blend
    {
        NSMutableArray* arr = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < [_dropsCount count]; i++)
        {
            [arr addObject:@[_oilNames[i], _dropsCount[i]]];
        }
        
        [_appDelegate.myBlends replaceObjectAtIndex:_selectedIndex withObject:@[[NSString stringWithFormat:@"%d",_selectedIndex], _blendName.text, arr]];
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myBlends forKey:@"_myBlends"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [SVProgressHUD showSuccessWithStatus:@"Successfully Saved"];
    }
    else {
        if (buttonIndex == 1)
        {
            _oilNames[_currentEditingIndex] = [alertView textFieldAtIndex:0].text;
            [_tableView reloadData];
        }
    }
}
- (IBAction)onAddBlend:(id)sender {
    [self cancelAllMenu];
    [self cancelRecommendOilname];
    [_blendName resignFirstResponder];
    if ([_blendName.text length] == 0 || [[_blendName.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Blend Name?!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }
    for (int i = 0; i < [_oilNames count]; i++)
    {
        NSString* c = _oilNames[i];
        if ([c length] == 0 || [[c stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0)
        {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Oil Name?!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
            
            NSIndexPath *indexPath = [[NSIndexPath alloc]init];
            indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            
            [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            UITableViewCell* cell = [_tableView cellForRowAtIndexPath:indexPath];
            
            [((UITextField*)[cell viewWithTag:3]) becomeFirstResponder];
            return;
        }
        
        else if ([_dropsCount[i] intValue] == 0)
        {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:@"0 Drops?!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
            
            NSIndexPath *indexPath = [[NSIndexPath alloc]init];
            indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            
            [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            UITableViewCell* cell = [_tableView cellForRowAtIndexPath:indexPath];
            
            [cell becomeFirstResponder];
            return;
        }
    }
    if (_selectedIndex == -1)
    {
        _selectedIndex = [_appDelegate.myBlends count];
        
        for (int i = 0; i < _selectedIndex; i++)
            if ([_appDelegate.myBlends[i][1] isEqualToString:_blendName.text] && !([_appDelegate.myBlends[i] count] > 3 && [_appDelegate.myBlends[i][3] isEqualToString:@"1"]))
            {
                [[[UIAlertView alloc]initWithTitle:@"Error" message:@"This blend name already exists!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
                _selectedIndex = -1;
                return;
            }
        
        NSMutableArray* arr = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < [_dropsCount count]; i++)
        {
            [arr addObject:@[_oilNames[i], _dropsCount[i]]];
        }
        
    
        [_appDelegate.myBlends addObject:@[[NSString stringWithFormat:@"%d", _selectedIndex], _blendName.text, arr]];
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myBlends forKey:@"_myBlends"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [SVProgressHUD showSuccessWithStatus:@"Successfully Added"];
    }
    else
    {
        if ([_appDelegate.myBlends[_selectedIndex][1] isEqualToString:_blendName.text]) ;
        else
        {
            for (int i = 0; i < [_appDelegate.myBlends count]; i++)
                if (i != _selectedIndex && [_appDelegate.myBlends[i][1] isEqualToString:_blendName.text])
                {
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:@"This blend name already exists!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
                    return;
                }
        }
        UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"This blend is already added.\nDo you want to update it?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 101;
        
        [alertView show];
    }
}
- (IBAction)onAddtoMyOils:(id)sender {
    [self cancelAllMenu];
    [self cancelRecommendOilname];
    [_blendName resignFirstResponder];
    if (_selectedIndex == -1)
    {
        [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please save this blend before adding to My Oils" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }

    NSLog(@"%@",_appDelegate.myOils);
    if ([_appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", -_selectedIndex - 1]])
    {
        [_appDelegate.myOils setObject:@[[NSNumber numberWithInt:1 + ([[_appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", -_selectedIndex - 1]][0] intValue])], @"A"] forKey:[NSString stringWithFormat:@"%d", -_selectedIndex - 1]];
            }
    else
        [_appDelegate.myOils setObject:@[[NSNumber numberWithInt:1], @"A"] forKey:[NSString stringWithFormat:@"%d", -_selectedIndex - 1]];
    
    [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOils forKey:@"_myOils"];
    [[NSUserDefaults standardUserDefaults]synchronize];


    /*
    NSMutableDictionary *_myOils = [[NSMutableDictionary alloc] initWithDictionary:_appDelegate.myOils];
    for (int i =0; i < [_dropsCount count]; i++) {
        
        NSInteger dropCount = [[_dropsCount objectAtIndex:i] integerValue];
        NSInteger oilNameIndex = [_wordsArr indexOfObject:_oilNames[i]];
        
        [_myOils setObject:@[[NSString stringWithFormat:@"%ld",(long)dropCount]] forKey:[NSString stringWithFormat:@"%ld",oilNameIndex+1]];
    }
    _appDelegate.myOils = [[NSMutableDictionary alloc] initWithDictionary:_myOils];
     */

    
    [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOils forKey:@"_myOils"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
    [SVProgressHUD showSuccessWithStatus:@"Successfully Added to My Oils"];
}
- (IBAction)onOilNameChanged:(id)sender {
    [self cancelAllMenu];
    NSString* keyword = ((UITextField*)sender).text;
    for (int i = 0; i < [_wordsArr count]; i++)
        if ([_wordsArr[i] rangeOfString:keyword options:NSCaseInsensitiveSearch].location == 0)
        {
            _flagArr[i] = @"1";
        }
        else _flagArr[i] = @"0";
    
    [((UITableView*)([[sender superview] viewWithTag:8934])) reloadData];
}
- (IBAction)onOilNameEditingEnd:(id)sender {
    UITextField* view = (UITextField*)sender;
    [(UITableView*)([view.superview viewWithTag:8934]) setHidden:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self cancelAllMenu];
    [_blendName resignFirstResponder];
    if (tableView.tag == 8934)
    {
        UITextField* oilText = (UITextField*)[tableView.superview viewWithTag:3];
        [oilText setText:_wordsArr[indexPath.row]];
        tableView.hidden = YES;
    }
    else{
        [self cancelRecommendOilname];
    }
}
-(void)cancelRecommendOilname
{
    if (_currentEditCell)
    {
        [[_currentEditCell viewWithTag:8934]setHidden:YES];
        [[_currentEditCell viewWithTag:3] resignFirstResponder];
    }
    _currentEditCell = nil;
}
- (IBAction)onBlendNameChanged:(id)sender {
    [self cancelAllMenu];
    [self cancelRecommendOilname];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (scrollView.tag == 970)
    {

        CGFloat targetX = scrollView.contentOffset.x + velocity.x * 60.0;
        if (targetX < 30)
        {
//            [self cancelAllMenu];
            [self cancelRecommendOilname];
            
            targetContentOffset->x = 0;
            NSArray* array = [_tableView visibleCells];
            
            for (UITableViewCell* cell in array)
            {
                if ([cell viewWithTag:970] != scrollView)
                    [((UIScrollView*)[cell viewWithTag:970]) setContentOffset:CGPointMake(45, 0) animated:YES];
            }
        }
        else
        {
            //[self cancelAllMenu];
            [self cancelRecommendOilname];
            
            targetContentOffset->x = 45;
            NSArray* array = [_tableView visibleCells];
            
            for (UITableViewCell* cell in array)
            {
                if (cell != scrollView.superview)
                [((UIScrollView*)[cell viewWithTag:970]) setContentOffset:CGPointMake(45, 0) animated:YES];
            }
        }
    }
}
-(void)onTapScrollViewCell:(UIGestureRecognizer*)recog
{
    UIScrollView* scrollView = (UIScrollView*)(recog.view);
    
    NSIndexPath* indexPath = [_tableView indexPathForCell:((UITableViewCell*)(scrollView.superview))];
    [self tableView:_tableView didSelectRowAtIndexPath:indexPath];
}
- (IBAction)onDeleteBlend:(id)sender {
    
    if (_selectedIndex < 0) {
        [SVProgressHUD showErrorWithStatus:@"Blend not saved"];
    } else {
        [_blendName resignFirstResponder];
        [self cancelRecommendOilname];
        UIAlertView* alertview = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure to delete this blend?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertview.tag = 107;
        [alertview show];
        
    }

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    UIView* view = touch.view;
    while (view)
    {
        view = [view superview];
        if (view.tag == 8934) break;
    }
    
    if (view) return NO;
    return YES;
}

@end
