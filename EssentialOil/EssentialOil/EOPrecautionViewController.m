//
//  EOPrecautionViewController.m
//  EssentialOil
//
//  Created by Steve on 2/16/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "AppDelegate.h"
#import "EOPrecautionViewController.h"
@interface EOPrecautionViewController()
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@end
@implementation EOPrecautionViewController
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y <= 0)
    {
        [self dismissViewControllerAnimated:YES      completion:nil];
    }
}

-(void)viewDidLoad
{
    int cellHeight = 40;
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    CGSize maximumLabelSize = CGSizeMake(220, FLT_MAX);
    
    NSLog(@"%@",_precautionIDs);
    NSArray* data;
    
    if (_mode == 1)
    {
        NSArray* images = @[@"ES_Precaution1_Manufacture.png",
                            @"ES_Precaution2_Inhaling.png",
                            @"ES_Precaution3_Dilute.png",
                            @"ES_Precaution4_Sun.png",
                            @"ES_Precaution5_InternalUse.png",
                            @"ES_Precaution6_Flame.png",
                            @"ES_Precaution7_Sweating.png",
                            @"ES_Precaution8_Pregnant.png",
                            @"ES_Precaution9_Diabetics.png",
                            @"ES_Precaution10_HighDose.png",
                            @"ES_Precaution11_Pills.png",
                            @"ES_Precaution12_BloodThinning.png"
                            ];
        int y = 30;
        
        for (int i = 0; i < [_precautionIDs count]; i++)
        {
            UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(100, y + 5, 180, cellHeight)];
            [label setFont:[UIFont fontWithName:@"Helvetica" size:12]];

            UIImageView* imageView = [[UIImageView alloc]initWithFrame:CGRectMake(40, y + 5, 40, cellHeight)];
            int s = [_precautionIDs[i] intValue] - 1;
            [imageView setImage:[UIImage imageNamed:images[s]]];
            
            [label setText:appDelegate.preCaution[s][1]];
            label.numberOfLines = 10;
            CGSize size = [label sizeThatFits:CGSizeMake(180, CGFLOAT_MAX)];
            
            CGRect rect = label.frame;
            rect.size.height = size.height;
            label.frame = rect;
            
            NSLog(@"%@",NSStringFromCGSize(size));
            
            [_mainScrollView addSubview:imageView];
            [_mainScrollView addSubview:label];

            y += ((size.height > 40) ? size.height : cellHeight) + 10;
            
            if ([_precautionIDs[i] intValue] == 1) {
                
                for (int j = 0 ; j < 3; j++) {
                
                    NSArray* images = @[@"ES_Precaution1_Children.png",
                                        @"ES_Precaution1_Eyes.png",
                                        @"ES_Precaution1_Dilute.png"];
                    
                    NSArray* precautionText = @[@"Keep out of reach of children.",
                                                @"Avoid contact with eyes, inner ears, and sensitive areas.",
                                                @"Dilute with coconut, olive, or other carrier oil when applying topically on sensitive areas."];
                    
                    UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(100, y + 5, 180, cellHeight)];
                    [label setFont:[UIFont fontWithName:@"Helvetica" size:12]];
                    UIImageView* imageView = [[UIImageView alloc]initWithFrame:CGRectMake(40, y + 5, 40, cellHeight)];
                    [imageView setImage:[UIImage imageNamed:[images objectAtIndex:j]]];
                    
                    [label setText:[precautionText objectAtIndex:j]];
                    label.numberOfLines = 10;
                    CGSize size = [label sizeThatFits:CGSizeMake(180, CGFLOAT_MAX)];
                    
                    CGRect rect = label.frame;
                    rect.size.height = size.height;
                    label.frame = rect;
                    
                    [_mainScrollView addSubview:imageView];
                    [_mainScrollView addSubview:label];
                    y += ((size.height > 40) ? size.height : cellHeight) + 10;
                }
            }
        }
    }
    else
    {
        NSLog(@"%@", _precautionIDs);
        NSArray* initials = @[@"A", @"TN", @"TS", @"TD", @"I"];
        int y = 80;
        for (int i = 0; i < [_precautionIDs count]; i++)
        {
            UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(80, y + 5, 220, cellHeight)];
            UILabel* iconLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, y + 5, 40, 40)];
            int s = [_precautionIDs[i] intValue] - 1;
            
            iconLabel.layer.borderWidth = 5;
            iconLabel.layer.borderColor = [[UIColor colorWithWhite:0.5 alpha:1] CGColor];
            iconLabel.layer.masksToBounds = YES;
            iconLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1];
            iconLabel.text = initials[s];
            iconLabel.textAlignment = NSTextAlignmentCenter;
            iconLabel.layer.cornerRadius = cellHeight / 2;
            [iconLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:20]];
            
            [label setText:appDelegate.howToUse[s][3]];
            label.numberOfLines = 10;
            [label setFont:[UIFont fontWithName:@"Helvetica Light" size:13]];
            
            
            CGSize expectedLabelSize = [label.text sizeWithFont:label.font constrainedToSize:maximumLabelSize lineBreakMode:label.lineBreakMode];
            
            CGRect newFrame = label.frame;
            
            newFrame.size.height = expectedLabelSize.height;
            label.frame = newFrame;
            
            
            
            [_mainScrollView addSubview:iconLabel];
            [_mainScrollView addSubview:label];
            y += newFrame.size.height + 10;
        }
        
        CGRect rt = [_mainScrollView frame];
        rt.size.height += 40;
        _mainScrollView.contentSize = rt.size;
        _mainScrollView.contentOffset = CGPointMake(0, 40);
    }
}

@end
