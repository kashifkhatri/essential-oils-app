//
//  EOOilIndexViewController.h
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLSwipeForOptionsCell.h"
@interface EOOilIndexViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, TLSwipeForOptionsCellDelegate,UISearchBarDelegate>
-(void)onHome:(id)sender;
@end
