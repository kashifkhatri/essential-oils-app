//
//  EOMyOilsMyOilsViewController.h
//  EssentialOil
//
//  Created by Steve on 2/22/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLSwipeForOptionsCell.h"

@interface EOMyOilsMyOilsViewController : UIViewController<TLSwipeForOptionsCellDelegate,UITableViewDataSource,UITableViewDelegate>
-(IBAction)onHome:(id)sender;
@end
