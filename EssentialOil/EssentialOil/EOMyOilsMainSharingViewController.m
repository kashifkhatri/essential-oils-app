//
//  EOMyOilsMainSharingViewController.m
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EOMyOilsViewController.h"
#import "EOMyOilsMainSharingViewController.h"
#import "UIPlaceHolderTextView.h"
#import "AppDelegate.h"
#import "EOMyOilsShareListViewController.h"
#import "SVProgressHUD.h"
#import "EOMyNotesDetailViewController.h"

@interface EOMyOilsMainSharingViewController()
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *noteView;
@property NSArray* unitArr;
@property AppDelegate* appDelegate;
@end

@implementation EOMyOilsMainSharingViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    _appDelegate = [[UIApplication sharedApplication]delegate];
    
    _unitArr = @[@"Drops", @"Bottles"];
    _noteView.placeholder = @"Type Notes Here";
    _noteView.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1] CGColor];
    _noteView.layer.borderWidth = 1;
    _noteView.layer.cornerRadius = 5;
    _mainScrollView.contentSize = CGSizeMake(320, 600);
    [_contactName setText:_appDelegate.myContacts[_selectedContact][1][0]] ;
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}

- (IBAction)noteSaved:(id)sender {
    
    NSArray *array = [[NSArray alloc] initWithArray:sender];
    NSLog(@"%@",array);
    if ([array count] > 1) {
        _noteTitle = array[0];
        _noteDescription = array[1];
    }
    
    NSLog(@"%@",_appDelegate.myOilsShare);
}


- (IBAction)onPlus:(id)sender {
    [_countLabel setText:[NSString stringWithFormat:@"%02d", [_countLabel.text intValue] + 1]];
}
- (IBAction)onMinus:(id)sender {
    if ([_countLabel.text isEqualToString:@"01"]) return;
    [_countLabel setText:[NSString stringWithFormat:@"%02d", [_countLabel.text intValue] - 1]];
}
- (IBAction)onRightUnit:(id)sender {
    _unit = !_unit;
    [_unitLabel setText:_unitArr[_unit]];
    
}
- (IBAction)onLeftUnit:(id)sender {
    _unit = !_unit;
    [_unitLabel setText:_unitArr[_unit]];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    [UIView animateWithDuration:0.25 animations:^{
        CGRect rt = self.view.frame;
        rt.origin.y = 0;
        self.view.frame = rt;
    }];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [UIView animateWithDuration:0.25 animations:^{
        CGRect rt = self.view.frame;
        rt.origin.y = -200;
        self.view.frame = rt;
    }];
}
- (IBAction)onAdd:(id)sender {
    
    if ([_countLabel.text integerValue] < 1) {
        [SVProgressHUD showWithStatus:@"You don't have that many oils to share"];
    } else {
        
        
        id value = [_appDelegate.myOilsShare objectForKey:_selectedIndex];
        
        if (value == nil)
            value = [[NSMutableDictionary alloc]init];
        
        NSString *str;
        
        if (_noteTitle == nil && _noteDescription == nil) {
            [value setObject:@[_countLabel.text, [NSString stringWithFormat:@"%d", _unit], [NSString stringWithFormat:@""] ] forKey:[NSString stringWithFormat:@"%d", _selectedContact]];
        } else {
            if (_noteTitle != nil && _noteDescription != nil) {
                str = [NSString stringWithFormat:@"%@ %@",_noteTitle,_noteDescription];
            }
            if (_noteTitle != nil) {
                str = [NSString stringWithFormat:@"%@",_noteTitle];
            }
            if (_noteDescription != nil) {
                str = [NSString stringWithFormat:@"%@",_noteDescription];
            }
            [value setObject:@[_countLabel.text, [NSString stringWithFormat:@"%d", _unit], [NSString stringWithFormat:@"%@",str] ] forKey:[NSString stringWithFormat:@"%d", _selectedContact]];
        }
        
//        [value setObject:@[_countLabel.text, [NSString stringWithFormat:@"%d", _unit], [NSString stringWithFormat:@"%@ %@",_noteTitle, _noteDescription] ] forKey:[NSString stringWithFormat:@"%d", _selectedContact]];
//        
        [_appDelegate.myOilsShare setObject:value forKey:_selectedIndex];
        
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOilsShare forKey:@"_myOilsShare"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"%@",_appDelegate.myOilsShare);
        
        if (_parentVCX)
        {
            [self.navigationController popToViewController:_parentVCX animated:YES];
            [(EOMyOilsShareListViewController*)_parentVCX contactAdded:nil];
            return;
        }
     
        int c = [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue];
        if (c == 0) return;
        
        NSLog(@"%@", _appDelegate.myOils);
        
        NSLog(@"%@", [_appDelegate.myOils objectForKey:_selectedIndex]);
        
        if ([[_unitLabel text] isEqualToString:@"Bottles"]) {

            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[_appDelegate.myOils objectForKey:_selectedIndex]];
            [array replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:c - [_countLabel.text intValue]]];
            
            [_appDelegate.myOils setObject:array forKey:_selectedIndex];
        }
        
        EOMyOilsShareListViewController* vc = (EOMyOilsShareListViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilssharelist"];
        
        vc.selectedIndex = _selectedIndex;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)onAddNoteAction:(id)sender
{
    EOMyNotesDetailViewController *eOMyNotesDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
    eOMyNotesDetailViewController.isNoteSharing = true;
    eOMyNotesDetailViewController.noteID = -1;
    eOMyNotesDetailViewController.parentVCX = self;
    [self.navigationController pushViewController:eOMyNotesDetailViewController animated:YES];
}

@end
