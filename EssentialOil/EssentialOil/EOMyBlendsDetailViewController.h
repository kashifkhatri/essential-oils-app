//
//  EOMyBlendsDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 2/11/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TLSwipeForOptionsCell.h"
@interface EOMyBlendsDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, TLSwipeForOptionsCellDelegate, UIGestureRecognizerDelegate>
@property UIViewController* parentVCX;
@property int selectedIndex;
@property AppDelegate* appDelegate;
@end
