//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyOilsViewController.h"
#import "EOMyOilsMyOilsViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "AppDelegate.h"
#import "EOMyOilsAddNewOilViewController.h"
#import "EOMyOilsShareListViewController.h"
#import "EOMyBlendsDetailViewController.h"

#define kCatchWidth 245 / 5 * _buttonCount

@interface EOMyOilsMyOilsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property AppDelegate* appDelegate;
@property NSMutableArray* keys;

@end
@implementation EOMyOilsMyOilsViewController
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadTableData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    _mainTableView.delegate = self;
    _mainTableView.dataSource = self;
    
    NSLog(@"%@",_appDelegate.myBlends);
    
}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_keys count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString* text;
    NSInteger stockCount = 0;
    
    int leftPos = 0 + [[UIScreen mainScreen]bounds].size.width - IMAGE_SIZE - 10;
    
    NSLog(@"%@",_keys);
    NSLog(@"%@",_appDelegate.myOils);
    
    if ([_keys[indexPath.row] hasPrefix:@"N_"])
    {
        text = [_appDelegate.myOils objectForKey:_keys[indexPath.row]][1];
        stockCount = [[_appDelegate.myOils objectForKey:_keys[indexPath.row]][0] integerValue];
    }
    else {
        int index = [_keys[indexPath.row] intValue];

        if (index > 299990)
        {
            NSLog(@"%@",_appDelegate.myBlends);
            NSString *strIndex = [NSString stringWithFormat:@"%d",index];
         
            NSInteger indexKey = [[NSString stringWithFormat:@"%ld",[strIndex integerValue] - 300000] integerValue];
            
            NSArray *array = _appDelegate.myBlends[-indexKey - 1];
            
            NSLog(@"%@",[_appDelegate.myOils objectForKey:strIndex]);
            
            stockCount = [[[_appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%ld",(long)indexKey]] objectAtIndex:0] intValue];
            
            //for (int i = 0; i < [array[2] count]; i++)
            //{
            //    stockCount += [array[2][i][1] integerValue];
            //}
            
            text = _appDelegate.myBlends[-indexKey - 1][1];
            
            NSLog(@"%@",_appDelegate.myOilsShare);
            NSMutableDictionary *_dataDict = [[NSMutableDictionary alloc]initWithDictionary:[_appDelegate.myOilsShare objectForKey:[NSString stringWithFormat:@"%ld",(long)indexKey]]];
            if ([[_dataDict allKeys] count] == 0) {
                NSLog(@"Empty %@",_dataDict);
            } else {
                
                UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, 29, 19)];
                noteImageView.tag = 1003;
                noteImageView.userInteractionEnabled = NO;
                [noteImageView setImage:[UIImage imageNamed:@"ES_IconContacts.png"]];
                [cell.scrollView addSubview:noteImageView];
                leftPos -= IMAGE_SIZE + 15;
                cell.scrollView.clipsToBounds = YES;
                //[noteImageView setBackgroundColor:[UIColor blueColor]];
            }
            
        }
        else {
            //NSLog(@"--- %d", index);
            text = _appDelegate.oilNames[index - 1][1];
            NSString *strIndex = [NSString stringWithFormat:@"%d",index];
            stockCount =[[_appDelegate.myOils objectForKey:strIndex][0] intValue];
            //NSLog(@"--- %@",_appDelegate.oilNames[index - 1]);
            
            NSLog(@"%@",_appDelegate.myOilsShare);
            NSMutableDictionary *_dataDict = [[NSMutableDictionary alloc]initWithDictionary:[_appDelegate.myOilsShare objectForKey:[_keys objectAtIndex:indexPath.row]]];
            if ([[_dataDict allKeys] count] == 0) {
                NSLog(@"Empty %@",_dataDict);
            } else {
                
                UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, 29, 19)];
                noteImageView.tag = 1003;
                noteImageView.userInteractionEnabled = NO;
                [noteImageView setImage:[UIImage imageNamed:@"ES_IconContacts.png"]];
                [cell.scrollView addSubview:noteImageView];
                leftPos -= IMAGE_SIZE + 15;
                cell.scrollView.clipsToBounds = YES;
                //[noteImageView setBackgroundColor:[UIColor blueColor]];
            }
        }
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%ld %@",(long)stockCount,text];
    cell.delegate = self;
    cell.indexPath = indexPath;
    
    return cell;
}
-(void)reloadTableData
{
    _appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSLog(@"%@",_appDelegate.myOils);
    NSLog(@"%@",_appDelegate.myNotes);
    
    _keys = [[NSMutableArray alloc]initWithArray:[_appDelegate.myOils allKeys]];
    
    NSLog(@"%@",_keys);
    NSLog(@"%@",_appDelegate.myOils);
    
    NSMutableDictionary *arraySort = [[NSMutableDictionary alloc] init];
    
    for (int i =0; i < [_keys count]; i++) {
        NSString *text=nil;

        if ([_keys[i] hasPrefix:@"N_"])
        {
            text = [_appDelegate.myOils objectForKey:_keys[i]][1];
        } else {
            int index = [_keys[i] intValue];
            if (index > 0)
            {
                text = _appDelegate.oilNames[index - 1][1];
            }
            else {
                text = _appDelegate.myBlends[-index - 1][1];
                NSString *str = [NSString stringWithFormat:@"%ld",300000 + [_keys[i] integerValue]];
                [_keys replaceObjectAtIndex:i withObject:str];
            }
        }
        text = [text lowercaseString];
        [arraySort setObject:text forKey:_keys[i]];
    }
    NSLog(@"%@",arraySort);
    
    NSMutableArray *_tempKeys = [[NSMutableArray alloc] initWithArray:[arraySort keysSortedByValueUsingSelector:@selector(compare:)]];
    
    NSLog(@"%@",_tempKeys);

    [_keys removeAllObjects];
    
    _keys = [[NSMutableArray alloc] initWithArray:_tempKeys];
    
    /*
    for (int i =0; i < [_tempKeys count]; i++) {
        
        
        if([[_tempKeys objectAtIndex:i] integerValue] > 299990)
        {
            NSString *blendKey = [_tempKeys objectAtIndex:i];
            //NSString *str = [NSString stringWithFormat:@"%ld",[_tempKeys[i] integerValue] - 300000];
            //[_tempKeys replaceObjectAtIndex:i withObject:str];
            [_keys addObject:blendKey];
        } else {

            NSString *str = [_tempKeys objectAtIndex:i];
            NSLog(@"%@",str);
            NSLog(@"%@",[arraySort objectForKey:str]);
            [_keys addObject:str];
        }        

    }
     */
    NSLog(@"%@",_keys);

    [_mainTableView reloadData];
}
-(void)onSelectCell:(TLSwipeForOptionsCell*)cell
{
    if ([[_keys objectAtIndex:cell.indexPath.row] integerValue] > 29000) {
        
        NSInteger index = [[_keys objectAtIndex:cell.indexPath.row] integerValue];
        
        int finalIndex = 300000 - (int)index - 1;
        
        NSLog(@"%ld",(long)cell.indexPath.row);
        NSLog(@"%@",_keys);
        
        EOMyOilsShareListViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myoilssharelist"];
        
        NSLog(@"%@",_keys);
        
        viewController.selectedIndex = [NSString stringWithFormat:@"%ld",index];
        //_keys[cell.indexPath.row];
        
        [self.navigationController pushViewController:viewController animated:YES ];
        
        /*
        EOMyBlendsDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myblendsdetailview"];
        viewController.appDelegate = _appDelegate;
        viewController.selectedIndex = finalIndex;
        viewController.parentVCX = self;
        [self.navigationController pushViewController:viewController animated:YES];
         */
        
    } else {
        
        EOMyOilsShareListViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myoilssharelist"];
        
        NSLog(@"%@",_keys);
        
        viewController.selectedIndex = _keys[cell.indexPath.row];
        
        [self.navigationController pushViewController:viewController animated:YES ];
    }
}

- (IBAction)onNavNotes:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_mynotes" object:nil];
}
- (IBAction)onMyContacts:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_mycontacts" object:nil];
    
}
- (IBAction)onMyBlends:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_myblends" object:nil];
    
}
- (IBAction)onNewOil:(id)sender {
    EOMyOilsAddNewOilViewController* viewController = (EOMyOilsAddNewOilViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsaddnewoil"];
    viewController.parentVCX = self;
    
    [self.navigationController pushViewController: viewController animated:YES];
    
}
@end
