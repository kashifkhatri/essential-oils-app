//
//  EOSettingsViewController.h
//  EssentialOil
//
//  Created by Steve on 2/24/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface EOSettingsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate,UIAlertViewDelegate>

@end
