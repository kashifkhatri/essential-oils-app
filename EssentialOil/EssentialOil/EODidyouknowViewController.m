//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EODidYouKnowDetailViewController.h"
#import "EOMenuViewController.h"
#import "EODidyouknowViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "AppDelegate.h"
#import "EOCommonUseDetailViewController.h"
@interface EODidyouknowViewController ()
@property (weak, nonatomic) IBOutlet UITabBarItem *barButton1;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property AppDelegate* appDelegate;

@end

@implementation EODidyouknowViewController
@synthesize appDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    [_barButton1 setImage:[UIImage imageNamed:@"ES_IconCommonUses30"]];
    // Do any additional setup after loading the view.
    appDelegate = [UIApplication sharedApplication].delegate;
    NSLog(@"%@", appDelegate.tips);
}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMenuViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [appDelegate.tips count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//    cell.buttonCount = 0;
//    cell.textLabel.text = [NSString stringWithFormat:@"%d. %@", indexPath.row + 1, appDelegate.tips[indexPath.row][1]];
//    cell.delegate = self;
//    cell.indexPath = indexPath;
//    cell.scrollView.contentSize = cell.frame.size;
//    cell.scrollViewButtonView.frame = CGRectMake(0,0,0,0);
//    return cell;
    
    
    [cell.scrollView removeFromSuperview];
    [[cell.contentView viewWithTag:2003]removeFromSuperview];
    UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 280, 49)];
    label.tag = 2003;
    [label setText:[NSString stringWithFormat:@"%d. %@", indexPath.row + 1, appDelegate.tips[indexPath.row][1]]];
    [cell.contentView addSubview:label];
    cell.delegate = self;
    cell.indexPath = indexPath;
    
    return cell;

}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
- (IBAction)onSearchButtonClicked:(id)sender {
    if (_searchBar.hidden)
    {
        [UIView animateWithDuration:0.1 animations:^{
            _searchBar.text = @"";
            [_searchBar setHidden:NO];
            [_searchBar setFrame:CGRectMake(0, 64, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y += 44;
            rt.size.height -= 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
        }];
    }
    else
    {
            [_searchBar resignFirstResponder];        
        [UIView animateWithDuration:0.1 animations:^{
            [_searchBar setFrame:CGRectMake(0, 20, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y -= 44;
            rt.size.height += 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
            [_searchBar setHidden:YES];
        }];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EODidYouKnowDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"didyouknowdetail"];
    
    viewController.parentVCX = self;
    viewController.selectedIndex = indexPath.row;
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}
-(void)onSelectCell:(TLSwipeForOptionsCell*)cell
{
    EODidYouKnowDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"didyouknowdetail"];

    viewController.parentVCX = self;
    viewController.selectedIndex = cell.indexPath.row;
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)onNavClicked:(id)sender {
    NSArray* stringArr = @[@"Nav_OilIndex", @"Nav_CommonUses", @"Nav_EssentialOilBasics", @"Nav_MyFavourites", @"Nav_MyOils"];
    int ID = ((UIButton*)sender).tag - 5600;
    [[NSNotificationCenter defaultCenter]postNotificationName:stringArr[ID] object:nil];
}
@end
