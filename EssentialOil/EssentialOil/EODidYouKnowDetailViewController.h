//
//  EODidYouKnowDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EODidyouknowViewController.h"
@interface EODidYouKnowDetailViewController : UIViewController
@property EODidyouknowViewController* parentVCX;
@property int selectedIndex;
@end

