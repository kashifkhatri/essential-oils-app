//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EOMenuViewController.h"
#import "EOCommonUseViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "EOCommonUseDetailViewController.h"
#import "EOMyNotesDetailViewController.h"
#import "SVProgressHUD.h"
#import "AppDelegate.h"
@interface EOCommonUseViewController ()
@property (weak, nonatomic) IBOutlet UITabBarItem *barButton1;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property AppDelegate* appDelegate;
@property NSMutableArray* filterArr;
@property NSMutableArray* indexPathArray;
@property NSMutableArray* commonUsesStringArray;
@property int buttonCount;
@end

@implementation EOCommonUseViewController

- (void)viewDidLoad {
        _buttonCount = 4;
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_barButton1 setImage:[UIImage imageNamed:@"ES_IconCommonUses30"]];
    
    _appDelegate = [[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    _filterArr = [[NSMutableArray alloc]init];
    int count =[_appDelegate.commonUses count];
    _indexPathArray = [[NSMutableArray alloc]init];
    _commonUsesStringArray = [[NSMutableArray alloc]init];
    NSMutableArray* indexArr = [[NSMutableArray alloc]init];
    
    int currentIndex = 0;
    
    for (int i = 0; i < count; i++)
    {

        NSLog(@"%@",_appDelegate.commonUses[i]);
        NSLog(@" ==== ");
        [_filterArr addObject:@"1" ];
        [_commonUsesStringArray addObject:_appDelegate.commonUses[i][2]];
        
//        if (currentIndex == 184) {
//            currentIndex = 185;
//        } else if (currentIndex == 284) {
//            currentIndex = 285;
//        } else if (currentIndex == 302) {
//            currentIndex = 303;
//        }
        
        [indexArr addObject:[NSNumber numberWithInt:i]];

        currentIndex++;
    }
    
    NSLog(@"%@",indexArr);
    
    for (int i = 0; i < count - 1; i++)
        for (int j = i + 1; j < count; j++)
        {
            if ([_commonUsesStringArray[i] caseInsensitiveCompare:_commonUsesStringArray[j]] == NSOrderedDescending)
            {
                NSString* temp = _commonUsesStringArray[i];
                _commonUsesStringArray[i] = _commonUsesStringArray[j];
                _commonUsesStringArray[j] = temp;
                
                NSNumber* temp1 = indexArr[i];
                indexArr[i] = indexArr[j];
                indexArr[j] = temp1;
            }
        }
    
    for (int i = 0; i < count; i++)
        [_indexPathArray addObject:indexArr[i]];
    
    NSLog(@"%@", _indexPathArray);
    NSLog(@"%@", _commonUsesStringArray);
    
    [_mainTableView reloadData];
    
    NSLog(@"%@",_appDelegate.commonUses);

}

- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMenuViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_appDelegate.commonUses count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if ([self tableView:tableView heightForRowAtIndexPath:indexPath] == 0) return cell;
    
    if (cell == nil) {
        cell = [[TLSwipeForOptionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.clipsToBounds = YES;
    cell.buttonCount = 4;
    cell.textLabel.text = _appDelegate.commonUses[[_indexPathArray[indexPath.row] intValue]][2];
    cell.indexPath = indexPath;

    int leftPos = 0 + [[UIScreen mainScreen]bounds].size.width - IMAGE_SIZE - 10 + kCatchWidth;
    
    for (UIView* view in cell.scrollView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    for (UIView* view in cell.scrollViewButtonView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    id note = ([_appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", [_indexPathArray[indexPath.row] intValue] + 1000]]);
    
    if ([_appDelegate.commonUsesFavourites objectForKey:[NSString                                                                   stringWithFormat:@"%d", [_indexPathArray[indexPath.row] intValue]]])
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, IMAGE_SIZE, IMAGE_SIZE)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_hearts.png"]];
        [cell.scrollView addSubview:noteImageView];
                cell.contentView.clipsToBounds= YES;
        leftPos -= IMAGE_SIZE + 15;
    }
    if (note)
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, IMAGE_SIZE, IMAGE_SIZE)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_IconNotepad.png"]];
        [cell.scrollView addSubview:noteImageView];
        cell.scrollView.clipsToBounds= YES;
        
        //[noteImageView addGestureRecognizer:rcongicne];
    }
    
    
    cell.delegate = self;
    
    
    NSMutableArray* imageButton = [[NSMutableArray alloc] initWithObjects:@"ES_SettingIconBack.png", @"ES_SettingIconFavorite.png", @"ES_SettingIconNotepad.png",@"ES_SettingIconTrash.png",nil];
    
    
    if ([_appDelegate.commonUsesFavourites objectForKey:[NSString                                                                   stringWithFormat:@"%d", [_indexPathArray[indexPath.row] intValue]]])
    {
        [imageButton replaceObjectAtIndex:1 withObject:[NSString stringWithFormat:@"ES_SettingIconFavorite1.png"]];
    }
                                   
    [cell.scrollViewButtonView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1]];
    
    [cell.scrollView setFrame:CGRectMake(0, 0, kCatchWidth, CGRectGetHeight(cell.bounds))];

    for (int i = 0; i < _buttonCount; i++)
    {
        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        moreButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
        int height = 49 - 20;
        if (height < 0) height=0;
        moreButton.frame = CGRectMake(kCatchWidth * 1.0 / _buttonCount * i + 10, 10, kCatchWidth * 1.0 / _buttonCount - 20, height);
        [moreButton setImage:[UIImage imageNamed:imageButton[i]] forState:UIControlStateNormal];
        [moreButton setTag:i + indexPath.row * 5];
        [moreButton addTarget:self action:@selector(userPressedMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.scrollViewButtonView addSubview:moreButton];
    }
    
    return cell;
}

-(void)userPressedMoreButton:(id)sender
{
    int tag = ((UIButton*)sender).tag;
    
    NSLog(@"%f",((UIButton*)sender).frame.origin.y);
    int buttonIndex = tag % 5;
    int rowIndex = tag / 5;
    

    UITableViewCell* cell;
    AppDelegate* appDelegate = _appDelegate;
    
    while (![sender isKindOfClass:[UITableViewCell class]])
        sender = [sender superview];
    cell = (UITableViewCell*)sender;
    switch (buttonIndex) {
        case 0:
            [[NSNotificationCenter defaultCenter]postNotificationName:TLSwipeForOptionsCellEnclosingTableViewDidBeginScrollingNotification object:nil];
            break;
        case 1:
        {
            int favButtonTag = buttonIndex + rowIndex * 5;
            
            UIButton *btn = (UIButton *)[cell viewWithTag:favButtonTag];
            
            if ([appDelegate.commonUsesFavourites objectForKey:[NSString                                                                   stringWithFormat:@"%d", [_indexPathArray[rowIndex] intValue]]])
            {
                [appDelegate.commonUsesFavourites removeObjectForKey:[NSString                                                                   stringWithFormat:@"%d", [_indexPathArray[rowIndex] intValue]]];
                [SVProgressHUD showSuccessWithStatus:@"Removed From Favorites!"];
                
                [btn setImage:[UIImage imageNamed:@"ES_SettingIconFavorite.png"] forState:UIControlStateNormal];
            }
            else
            {
                [appDelegate.commonUsesFavourites setObject:@"1" forKey:[NSString                                                                   stringWithFormat:@"%d", [_indexPathArray[rowIndex] intValue]]];
                [SVProgressHUD showSuccessWithStatus:@"Added To Favorites!"];
                [btn setImage:[UIImage imageNamed:@"ES_SettingIconFavorite1.png"] forState:UIControlStateNormal];
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:appDelegate.commonUsesFavourites forKey:@"_commonUsesFavourites"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [_mainTableView reloadRowsAtIndexPaths:@[((TLSwipeForOptionsCell*)cell).indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        }
            //            [appDelegate.oilIndexFavourites setObject:@"1" forKey:[NSString                                                                   stringWithFormat:@"%d", rowIndex]];
            break;
        case 2:
        {
            EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
            
            viewController.noteID = [_indexPathArray[rowIndex] intValue] + 1000;
            viewController.parentVCX = self;
            viewController.stringTitle = _appDelegate.commonUses[[_indexPathArray[rowIndex] intValue]][2];
            [self presentViewController:viewController animated:YES completion:nil];
            break;
        }
        case 3:
        {
            int s;
            if ([appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", rowIndex + 1]])
            {
                s = [[appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", rowIndex + 1]][0] intValue];
            }
            else s = 0;
            
            s++;
            
            [appDelegate.myOils setObject:@[[NSString stringWithFormat:@"%d", s]] forKey:[NSString stringWithFormat:@"%d", rowIndex + 1]];
            
            [[NSUserDefaults standardUserDefaults]setObject:appDelegate.myOils forKey:@"_myOils"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            break;
        }
        case 4:
            if ([appDelegate.oilNames[rowIndex] count] > 7)  // This Oil is Removed
                appDelegate.oilNames[rowIndex][7] = @"1";
            else {
                NSMutableArray* arr= [[NSMutableArray alloc]initWithArray:appDelegate.oilNames[rowIndex]];
                while ([arr count] <= 7)
                    [arr addObject:@"1"];
                appDelegate.oilNames[rowIndex] = arr;
                [[NSUserDefaults standardUserDefaults]setObject:appDelegate.oilNames forKey:@"oilNames"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [self.mainTableView reloadData];
            }
            
        default:
            break;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_filterArr[indexPath.row] integerValue] == 1)
        return 49;
    else return 0;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSString* keyWord = [searchText lowercaseString];
    int count = [_appDelegate.commonUses count];
    for (int i = 0; i < count; i++)
    {
        if (([keyWord length] > 0 && [[_appDelegate.commonUses[[_indexPathArray[i] intValue]][2] lowercaseString] rangeOfString:keyWord].location == NSNotFound) || ([_appDelegate.commonUses[[_indexPathArray[i] intValue]] count] ==4 && [_appDelegate.commonUses[[_indexPathArray[i] intValue]][3] intValue] == 1))
        {
            _filterArr[i] = @"0";
        } else {
            _filterArr[i] = @"1";
        }
    }
    
    [_mainTableView reloadData];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
- (IBAction)onSearchButtonClicked:(id)sender {
    if (_searchBar.hidden)
    {
        [UIView animateWithDuration:0.1 animations:^{
            _searchBar.text = @"";
            [_searchBar setHidden:NO];
            [_searchBar setFrame:CGRectMake(0, 64, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y += 44;
            rt.size.height -= 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
        }];
    }
    else
    {
            [_searchBar resignFirstResponder];        
        [UIView animateWithDuration:0.1 animations:^{
            [_searchBar setFrame:CGRectMake(0, 20, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y -= 44;
            rt.size.height += 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
            [_searchBar setHidden:YES];
        }];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EOCommonUseDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"commonusedetailview"];
    
    
    
    int rowIndex = [_indexPathArray[indexPath.row] intValue] + 1;
    viewController.selectedIndex = rowIndex - 1;
    int oilCount = [_appDelegate.oilNames count];
    
    
    NSString* temp;
    NSArray* tempArr;
    NSMutableArray* usedArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < oilCount; i++)
    {
        temp = _appDelegate.oilNames[i][5];
        
        tempArr = [temp componentsSeparatedByString:@","];
        int flag = 0;
        for (NSString* c in tempArr)
        {
            int p = [c intValue];
            if (p < 1) continue;
            
            if(rowIndex == p)
            {
                flag = 1; break;
            }
        }
        
        if (flag)
            [usedArr addObject:_appDelegate.oilNames[i][1]];
    }
    
    viewController.arrUsedOils = [[NSArray alloc]initWithArray:usedArr];
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}
-(void)onSelectCell:(TLSwipeForOptionsCell*)cell
{
    EOCommonUseDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"commonusedetailview"];
    
    int rowIndex = [_indexPathArray[cell.indexPath.row] intValue];
    viewController.selectedIndex = rowIndex;
    int oilCount = [_appDelegate.oilNames count];
    
    if (rowIndex > 183 && rowIndex < 285) {
        rowIndex = rowIndex + 2;
    }
    if (rowIndex > 283 && rowIndex < 302) {
        rowIndex = rowIndex + 3;
    }
    if (rowIndex > 301) {
        rowIndex = rowIndex + 4;
    }


    
    
    NSString* temp;
    NSArray* tempArr;
    NSMutableArray* usedArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < oilCount; i++)
    {
        temp = _appDelegate.oilNames[i][5];
        
        tempArr = [temp componentsSeparatedByString:@","];
        int flag = 0;
        for (NSString* c in tempArr)
        {
            int p = [c intValue];
            NSLog(@"%d",p);
            if (p < 1) continue;
            
            if(rowIndex == p)
            {
                flag = 1; break;
            }
        }
        
        if (flag)
            [usedArr addObject:_appDelegate.oilNames[i][1]];
    }
    
    viewController.arrUsedOils = [[NSArray alloc]initWithArray:usedArr];
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}
- (IBAction)onNavClicked:(id)sender {
    NSArray* stringArr = @[@"Nav_OilIndex", @"Nav_DidYouKnow", @"Nav_EssentialOilBasics", @"Nav_MyFavourites", @"Nav_MyOils"];
    int ID = ((UIButton*)sender).tag - 5600;
    [[NSNotificationCenter defaultCenter]postNotificationName:stringArr[ID] object:nil];
}
@end
