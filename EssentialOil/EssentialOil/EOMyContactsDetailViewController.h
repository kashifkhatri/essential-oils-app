//
//  EODidYouKnowDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EOMyContactsViewController.h"
@interface EOMyContactsDetailViewController : UIViewController<UIAlertViewDelegate, UIScrollViewDelegate,UITextFieldDelegate>

@property EOMyContactsViewController* parentVCX;
@property int selectedIndex;
@end

