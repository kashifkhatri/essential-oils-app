//
//  EOMyOilsShareViewController.m
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyOilsShareViewController.h"
#import "AppDelegate.h"
#import "EOMyOilsAddContactViewController.h"
#import "EOMyOilsNewContactViewController.h"
#import "EOMyOilsViewController.h"

@interface EOMyOilsShareViewController()
@property (weak, nonatomic) IBOutlet UILabel *oilNameLabel;
@property AppDelegate* appDelegate;

@end

@implementation EOMyOilsShareViewController

-(void)viewDidLoad
{
    NSString* text;
    _appDelegate = [[UIApplication sharedApplication]delegate];
    if ([_selectedIndex hasPrefix:@"N_"])
        text = [_appDelegate.myOils objectForKey:_selectedIndex][1];
    else {
        int index = [_selectedIndex intValue];
        if (index > 0)
            text = _appDelegate.oilNames[index - 1][1];
        else
            text = _appDelegate.myBlends[-index - 1][1];
        
    }
    
    _oilNameLabel.text = text;

}

- (IBAction)onAddcontact:(id)sender {
    EOMyOilsAddContactViewController* vc = (EOMyOilsAddContactViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsaddcontact"];
    
    vc.selectedIndex = _selectedIndex;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onNewContact:(id)sender {
    EOMyOilsNewContactViewController* vc = (EOMyOilsNewContactViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsnewcontactvc"];
    
    vc.selectedIndexString = _selectedIndex;
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}
@end
