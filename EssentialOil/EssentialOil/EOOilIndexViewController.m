//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOOilIndexViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "EOMenuViewController.h"
#import "AppDelegate.h"
#import "EOOilIndexDetailViewController.h"
#import "EOMyNotesDetailViewController.h"
#import "SVProgressHUD.h"


@interface EOOilIndexViewController ()
@property (weak, nonatomic) IBOutlet UITabBarItem *barButton1;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property NSMutableArray* rowHidden, *wordsArr;
@property AppDelegate* appDelegate;
@property int buttonCount;
@end

@implementation EOOilIndexViewController
@synthesize appDelegate;

-(void)viewDidAppear:(BOOL)animated
{
    [_mainTableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _buttonCount = 5;
    [_barButton1 setImage:[UIImage imageNamed:@"ES_IconCommonUses30"]];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    _rowHidden = [[NSMutableArray alloc]init];
    _wordsArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < [appDelegate.oilNames count]; i++)
    {
        [_rowHidden addObject:@"1"];
        [_wordsArr addObject:[appDelegate.oilNames[i][1] lowercaseString]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appDelegate.oilNames[indexPath.row] count] > 7 && [appDelegate.oilNames[indexPath.row][7] integerValue] == 1)  // This Oil is Removed
        return 0;
    
    if ([_rowHidden[indexPath.row] integerValue] == 1)
       return 49;
    return 0;
}
-(void)onSelectCell:(TLSwipeForOptionsCell *)cell
{
    EOOilIndexDetailViewController* viewController = (EOOilIndexDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"oilindexdetail"];
    viewController.selectedIndex = cell.indexPath.row;
//    [self.navigationController pushViewController:viewController
//                                         animated:YES];
    [self presentViewController:viewController animated:YES completion:nil];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [appDelegate.oilNames count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString*cellId = @"Cell";
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if ([self tableView:tableView heightForRowAtIndexPath:indexPath] == 0) return cell;
    
    if (cell == nil) {
        cell = [[TLSwipeForOptionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.clipsToBounds = YES;
    cell.buttonCount = 5;
    cell.textLabel.text = appDelegate.oilNames[indexPath.row][1];
    cell.indexPath = indexPath;
    
    int leftPos = 0 + [[UIScreen mainScreen]bounds].size.width - IMAGE_SIZE - 10 + kCatchWidth;
    
    id countOfMyOils = ([appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", indexPath.row + 1]][0]);
    
//    [[cell viewWithTag:1002] setHidden:YES];
//    [[cell viewWithTag:1003] setHidden:YES];

    for (UIView* view in cell.scrollView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    for (UIView* view in cell.scrollViewButtonView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    UITapGestureRecognizer* rcongicne = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onImageTap:)];
    
    rcongicne.numberOfTapsRequired = 1;

    NSLog(@"%@, %d", countOfMyOils, indexPath.row);
    
    
    if ([countOfMyOils integerValue] > 0)
    {
        UIImageView* noteMyOilsView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, IMAGE_SIZE, IMAGE_SIZE)];
        [noteMyOilsView setImage:[UIImage imageNamed:@"ES_IconOilBottles.png"]];
        noteMyOilsView.userInteractionEnabled = NO;
        [cell.scrollView addSubview:noteMyOilsView];
        cell.scrollView.clipsToBounds = YES;
        [noteMyOilsView addGestureRecognizer:rcongicne];
        noteMyOilsView.tag = 1002;
        leftPos -= IMAGE_SIZE + 15;
    }
    
    id note = ([appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", indexPath.row + 1]]);

    if ([appDelegate.oilIndexFavourites objectForKey:[NSString                                                                   stringWithFormat:@"%d", indexPath.row]])
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, IMAGE_SIZE, IMAGE_SIZE)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_hearts.png"]];
        [cell.scrollView addSubview:noteImageView];
        leftPos -= IMAGE_SIZE + 15;
        cell.scrollView.clipsToBounds = YES;
    }
    if (note)
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, IMAGE_SIZE, IMAGE_SIZE)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_IconNotepad.png"]];
        [cell.scrollView addSubview:noteImageView];
        cell.scrollView.clipsToBounds = YES;
        
        //[noteImageView addGestureRecognizer:rcongicne];
    }

    
    cell.delegate = self;
    
    
    NSMutableArray* imageButton = [[NSMutableArray alloc] initWithObjects:@"ES_SettingIconBack.png", @"ES_SettingIconFavorite.png", @"ES_SettingIconNotepad.png", @"ES_IconBlendsWhite.png",
                                   @"ES_SettingIconTrash.png", nil];
    
    // @[@"ES_SettingIconBack.png", @"ES_SettingIconFavorite.png", @"ES_SettingIconNotepad.png", @"ES_IconBlendsWhite.png",
    //                         @"ES_SettingIconTrash.png"];
    
    if ([appDelegate.oilIndexFavourites objectForKey:[NSString stringWithFormat:@"%d", indexPath.row]])
    {
        [imageButton replaceObjectAtIndex:1 withObject:[NSString stringWithFormat:@"ES_SettingIconFavorite1.png"]];
    }
    //
    
    [cell.scrollViewButtonView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1]];
    
    [cell.scrollView setFrame:CGRectMake(0, 0, kCatchWidth, CGRectGetHeight(cell.bounds))];
    if (indexPath.row == 3)
    {
        int s = 0;
        s = 1;
    }
    for (int i = 0; i < 5; i++)
    {
        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        moreButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
        int height = 49 - 20;
        if (height < 0) height=0;
        moreButton.frame = CGRectMake(kCatchWidth / 5.0f * i + 10, 10, kCatchWidth / 5.0f - 20, height);
        [moreButton setImage:[UIImage imageNamed:imageButton[i]] forState:UIControlStateNormal];
        [moreButton setTag:i + indexPath.row * 5];
        NSLog(@"%d",moreButton.tag);
        [moreButton addTarget:self action:@selector(userPressedMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.scrollViewButtonView addSubview:moreButton];
    }
    
    return cell;
}
-(void)onImageTap:(UITapGestureRecognizer*)sender
{
    NSLog(@"%d",  sender.view.tag);
}
-(void)userPressedMoreButton:(id)sender
{
    int tag = ((UIButton*)sender).tag;
    
    NSLog(@"%f",((UIButton*)sender).frame.origin.y);
    int buttonIndex = tag % 5;
    int rowIndex = tag / 5;
    
    UITableViewCell* cell;
    
    while (![sender isKindOfClass:[UITableViewCell class]])
        sender = [sender superview];
    cell = (UITableViewCell*)sender;
    switch (buttonIndex) {
        case 0:
            [[NSNotificationCenter defaultCenter]postNotificationName:TLSwipeForOptionsCellEnclosingTableViewDidBeginScrollingNotification object:nil];
            break;
        case 1:
        {
            int favButtonTag = buttonIndex + rowIndex * 5;
            
            UIButton *btn = (UIButton *)[cell viewWithTag:favButtonTag];
            
            if ([appDelegate.oilIndexFavourites objectForKey:[NSString                                                                   stringWithFormat:@"%d", rowIndex]])
            {
                [appDelegate.oilIndexFavourites removeObjectForKey:[NSString                                                                   stringWithFormat:@"%d", rowIndex]];
                [SVProgressHUD showSuccessWithStatus:@"Removed From Favorites!"];
                [btn setImage:[UIImage imageNamed:@"ES_SettingIconFavorite.png"] forState:UIControlStateNormal];
            }
            else
            {
                [appDelegate.oilIndexFavourites setObject:@"1" forKey:[NSString                                                                   stringWithFormat:@"%d", rowIndex]];
                [SVProgressHUD showSuccessWithStatus:@"Added To Favorites!"];
                [btn setImage:[UIImage imageNamed:@"ES_SettingIconFavorite1.png"] forState:UIControlStateNormal];
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:appDelegate.oilIndexFavourites forKey:@"_oilIndexFavourites"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [_mainTableView reloadData];
            
            break;
        }
//            [appDelegate.oilIndexFavourites setObject:@"1" forKey:[NSString                                                                   stringWithFormat:@"%d", rowIndex]];
            break;
        case 2:
        {
            EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
            
            viewController.noteID = rowIndex + 1;
            viewController.parentVCX = self;
            viewController.stringTitle = appDelegate.oilNames[rowIndex][1];
            [self presentViewController:viewController animated:YES completion:nil];
            break;
        }
        case 3:
        {
            int s;
            if ([appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", rowIndex + 1]])
            {
                s = [[appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%d", rowIndex + 1]][0] intValue];
            }
            else s = 0;
            
            s++;
            
            [appDelegate.myOils setObject:@[[NSString stringWithFormat:@"%d", s]] forKey:[NSString stringWithFormat:@"%d", rowIndex + 1]];
            
            [[NSUserDefaults standardUserDefaults]setObject:appDelegate.myOils forKey:@"_myOils"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [SVProgressHUD showSuccessWithStatus:@"Added To My Oils!"];
            break;
        }
        case 4:
            if ([appDelegate.oilNames[rowIndex] count] > 7)  // This Oil is Removed
                appDelegate.oilNames[rowIndex][7] = @"1";
            else {
                NSMutableArray* arr= [[NSMutableArray alloc]initWithArray:appDelegate.oilNames[rowIndex]];
                while ([arr count] <= 7)
                    [arr addObject:@"1"];
                appDelegate.oilNames[rowIndex] = arr;
                [[NSUserDefaults standardUserDefaults]setObject:appDelegate.oilNames forKey:@"oilNames"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [self.mainTableView reloadData];
                
                [SVProgressHUD showSuccessWithStatus:@"Removed Successfully!"];
            }
            
        default:
            break;
    }
}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMenuViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{

        
        NSString* keyWord = [searchText lowercaseString];
    
        for (int i = 0; i < [appDelegate.oilNames count]; i++)
        {
            if ([searchText length] > 0 && [_wordsArr[i] rangeOfString:keyWord].location == NSNotFound)
            {
                _rowHidden[i] = @"0";
            } else {
                _rowHidden[i] = @"1";
            }
        }
        
        [_mainTableView reloadData];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
- (IBAction)onSearchButtonClicked:(id)sender {
    if (_searchBar.hidden)
    {
        _searchBar.delegate = self;
        [UIView animateWithDuration:0.1 animations:^{

            [_searchBar setHidden:NO];
            [_searchBar setFrame:CGRectMake(0, 64, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y += 44;
            rt.size.height -= 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
            [_mainTableView reloadData];
        }];
        
    }
    else
    {
            [_searchBar resignFirstResponder];
        [UIView animateWithDuration:0.1 animations:^{
            [_searchBar setFrame:CGRectMake(0, 20, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y -= 44;
            rt.size.height += 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
            [_searchBar setHidden:YES];
            [_mainTableView reloadData];
        }];
    }
}
- (IBAction)onCommonUsesNavController:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"Nav_CommonUses" object:nil];
}
- (IBAction)onDidYouKnowNavController:(id)sender {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"Nav_DidYouKnow" object:nil];
}
- (IBAction)onEssentialOilBasicsNavController:(id)sender {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"Nav_EssentialOilBasics" object:nil];
}
- (IBAction)onMyFavouritesNavController:(id)sender {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"Nav_MyFavourites" object:nil];
}
- (IBAction)onMyOilsNavController:(id)sender {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"Nav_MyOils" object:nil];
}

@end
