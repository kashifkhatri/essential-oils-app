//
//  EODidYouKnowDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOEssentialOilBasicsDetailViewController.h"

@interface EOEssentialOilBasicsDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation EOEssentialOilBasicsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateText];
}
- (IBAction)onBackTip:(id)sender {
    _selectedIndex--;
    if(_selectedIndex < 0) _selectedIndex = [_appDelegate.generalInformation count] - 1;
    [self updateText];
}
- (IBAction)onNextTip:(id)sender {
    _selectedIndex++;
    if (_selectedIndex >= [_appDelegate.generalInformation count])
        _selectedIndex = 0;
    [self updateText];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onHome:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        [_parentVCX onHome:nil];
    }];
}
- (IBAction)onBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)updateText
{
    [_titleLabel setText:_appDelegate.generalInformation[_selectedIndex][1]];
    [_textLabel setText:_appDelegate.generalInformation[_selectedIndex][2]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
