//
//  CSVParser.h
//  CSVParser
//
//  Created by XinZhangZhe on 4/4/13.
//  Copyright (c) 2013 Xin ZhangZhe. All rights reserved.
//
// --- Headers --- ;
#import <Foundation/Foundation.h>

@interface CSVParser : NSObject

// Functions ;
- ( id ) initWithString : ( NSString* ) _csvString separator : ( NSString* ) _separator ;
- ( NSArray* ) Parse ;

@end
