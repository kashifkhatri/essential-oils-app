//
//  EOMyOilsShareDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 2/24/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOMyOilsShareDetailViewController : UIViewController
@property NSArray* dataArr;
@property NSString* selectedIndex;
@property NSInteger indexPathRow;
@end
