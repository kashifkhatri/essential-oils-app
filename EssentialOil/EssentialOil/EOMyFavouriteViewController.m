//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EOMenuViewController.h"
#import "EOCommonUseViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "AppDelegate.h"
#import "EOMyFavouriteViewController.h"
#import "EOOilIndexDetailViewController.h"
#import "EOCommonUseDetailViewController.h"
#import "EOMyContactsDetailViewController.h"
#import "EOMyBlendsDetailViewController.h"
#import "EOMyNotesDetailViewController.h"

@interface EOMyFavouriteViewController ()
@property (weak, nonatomic) IBOutlet UITabBarItem *barButton1;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property AppDelegate* appDelegate;
@property NSMutableArray *hiddenStatus;
@property NSMutableArray* oilNames, *commonUses, *myBlends, *myContacts, *myNotes;
@property NSArray* oilFavourites, *commonUsesFavourites, *blendsFavourites, *contactFavourites, *noteFavourites;

@property NSMutableArray* oilKeys, *commonUsesKeys, *blendsKeys, *contactKeys, *noteKeys;

@end

@implementation EOMyFavouriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_barButton1 setImage:[UIImage imageNamed:@"ES_IconCommonUses30"]];
    _appDelegate = [[ UIApplication sharedApplication]delegate];
     [self refreshData];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    [self refreshData];
    [_mainTableView reloadData];
}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMenuViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5; //oilIndexFavourites   , *commonUsesFavourites, *blendsFavourites, *contactFavourites, *noteFavourites;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return [_oilFavourites count];
        case 1:
            return [_commonUsesFavourites count];
        case 2:
            return [_blendsFavourites count];
        case 3:
            return [_contactFavourites count];
        case 4:
            return [_noteFavourites count];
            break;
    }
    return 0;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return @"Oil Index ";
        case 1:
            return @"Common Uses ";
        case 2:
            return @"Blends ";
        case 3:
            return @"Contacts ";
        case 4:
            return @"Notes ";
    }
    return @"";
}

/*
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *strSectionName = @"";
    switch (section)
    {
        case 0:
            strSectionName = @"Oil Index ";
        case 1:
            strSectionName = @"Common Uses ";
        case 2:
            strSectionName = @"Blends ";
        case 3:
            strSectionName = @"Contacts ";
        case 4:
            strSectionName = @"Notes ";
    }
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont fontWithName:@"Helvetica" size:18];
    lbl.text = strSectionName;
    lbl.textColor = [UIColor blackColor];
    lbl.alpha = 1.0;
    return lbl;
}
 */


-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;

    header.textLabel.textAlignment = NSTextAlignmentCenter;
    header.backgroundColor = [UIColor clearColor];
    header.textLabel.backgroundColor = [UIColor clearColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* keyWord = _searchBar.text;
    if ([keyWord length] == 0) return 40;
    NSString* text;
    switch (indexPath.section)
    {
        case 0:
            text = _oilNames[[_oilFavourites[[_oilKeys[indexPath.row] intValue]]intValue]];
            break;
        case 1:
            text = _commonUses[[_commonUsesFavourites[[_commonUsesKeys[indexPath.row] intValue]]intValue]];
            break;
        case 2:
            text = _myBlends[[_blendsFavourites[[_blendsKeys[indexPath.row] intValue]]intValue]];
            break;
        case 3:
            text = _myContacts[[_contactFavourites[[_contactKeys[indexPath.row] intValue]]intValue]];
            break;
        case 4:
            text = _myNotes[[_noteFavourites[[_noteKeys[indexPath.row] intValue]]intValue]];
    }

    if ([text rangeOfString:keyWord options:NSCaseInsensitiveSearch].location == NSNotFound)
        return 0;
    return 40;
                                                           
}
-(void)onSelectCell:(TLSwipeForOptionsCell *)cell
{
    NSIndexPath* indexPath = ((NSIndexPath*)cell.indexPath);
    NSLog(@"%d, %d", indexPath.row, indexPath.section);
    switch (indexPath.section) {
        case 0:
            {
                EOOilIndexDetailViewController* viewController = (EOOilIndexDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"oilindexdetail"];
                viewController.selectedIndex = [_oilFavourites[[_oilKeys[ indexPath.row] intValue]] intValue];
                //    [self.navigationController pushViewController:viewController
                //                                         animated:YES];
                [self presentViewController:viewController animated:YES completion:nil];
            }
            break;
        case 1:
            {
                EOCommonUseDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"commonusedetailview"];
                
                
                
                int rowIndex = [_commonUsesFavourites[[_commonUsesKeys[ indexPath.row] intValue]] intValue] + 1;
                viewController.selectedIndex = rowIndex - 1;
                int oilCount = [_appDelegate.oilNames count];
                
                
                NSString* temp;
                NSArray* tempArr;
                NSMutableArray* usedArr = [[NSMutableArray alloc]init];
                for (int i = 0; i < oilCount; i++)
                {
                    temp = _appDelegate.oilNames[i][5];
                    
                    tempArr = [temp componentsSeparatedByString:@","];
                    int flag = 0;
                    for (NSString* c in tempArr)
                    {
                        int p = [c intValue];
                        if (p < 1) continue;
                        
                        if(rowIndex == p)
                        {
                            flag = 1; break;
                        }
                    }
                    
                    if (flag)
                        [usedArr addObject:_appDelegate.oilNames[i][1]];
                }
                
                viewController.arrUsedOils = [[NSArray alloc]initWithArray:usedArr];
                [self.navigationController presentViewController:viewController animated:YES completion:nil];
            }
            break;
        case 2:
        {
            EOMyBlendsDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myblendsdetailview"];
            viewController.appDelegate = _appDelegate;
            viewController.selectedIndex = [_blendsFavourites[[_blendsKeys[ indexPath.row] intValue]] intValue];
            viewController.parentVCX = self;
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        }
        case 3:
        {
            EOMyContactsDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mycontactsdetail"];
            
            viewController.parentVCX = nil;
            viewController.selectedIndex = [_contactFavourites[[_contactKeys[ indexPath.row] intValue]] intValue];
            
            [self.navigationController presentViewController:viewController animated:YES completion:nil];
        }
        case 4:
        {
            EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
            
            viewController.noteID = [_noteFavourites [[_noteKeys [indexPath.row] intValue]] intValue];
            viewController.parentVCX = self;
//            viewController.stringTitle = _appDelegate.commonUses[[_indexPathArray[rowIndex] intValue]][2];

            [self.navigationController presentViewController:viewController animated:YES completion:nil];
        }
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            if ([_oilFavourites count] == 0) return 0;
            break;
        case 1:
            if ([_commonUsesFavourites count] == 0) return 0;
            break;
        case 2:
            if ([_blendsFavourites count] == 0) return 0;
            break;
        case 3:
            if ([_contactFavourites count] == 0) return 0;
            break;
        case 4:
            if ([_noteFavourites count] == 0) return 0;
            //            text = _appDelegate.[[[_appDelegate.blendsFavourites allKeys][indexPath.row] intValue] ][1];
            break;
    }
    return 40;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString* text = @"";
    switch (indexPath.section)
    {
        case 0:
            text = _oilNames[[_oilFavourites[[_oilKeys[indexPath.row] intValue] ] intValue]];
            break;
        case 1:
            text = _commonUses[[_commonUsesFavourites[[_commonUsesKeys[indexPath.row] intValue] ] intValue]];
            break;
        case 2:
            text = _myBlends[[_blendsFavourites[[_blendsKeys[indexPath.row] intValue] ] intValue]];
            break;
        case 3:
            text = _myContacts[[_contactFavourites[[_contactKeys[indexPath.row] intValue] ] intValue]];
            break;
        case 4:
            NSLog(@"%@",_myNotes);
            NSLog(@"%@",_noteFavourites);
            NSLog(@"%@",_noteKeys);
            text = [_myNotes objectAtIndex:indexPath.row];
            //text = _myNotes[[_noteFavourites[[_noteKeys[indexPath.row] intValue] ] intValue]];
//            text = _appDelegate.[[[_appDelegate.blendsFavourites allKeys][indexPath.row] intValue] ][1];
            break;
    }
    cell.textLabel.text = text;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.delegate = self;
    cell.indexPath = indexPath;
    return cell;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
- (IBAction)onSearchButtonClicked:(id)sender {
    if (_searchBar.hidden)
    {
        [UIView animateWithDuration:0.1 animations:^{
            _searchBar.text = @"";
            [_searchBar setHidden:NO];
            [_searchBar setFrame:CGRectMake(0, 64, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y += 44;
            rt.size.height -= 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
        }];
    }
    else
    {
        [_searchBar resignFirstResponder];
        [UIView animateWithDuration:0.1 animations:^{
            [_searchBar setFrame:CGRectMake(0, 20, [[UIScreen mainScreen]bounds].size.width, 44)];
            CGRect rt = _mainTableView.frame;
            rt.origin.y -= 44;
            rt.size.height += 44;
            [_mainTableView setFrame:rt];
            [((UIButton*)sender) setEnabled:NO];
        } completion:^(BOOL finished) {
            [((UIButton*)sender) setEnabled:YES];
            [_searchBar setHidden:YES];
        }];
    }
    
}



- (IBAction)onNavClicked:(id)sender {
    NSArray* stringArr = @[@"Nav_OilIndex", @"Nav_CommonUses", @"Nav_DidYouKnow", @"Nav_EssentialOilBasics", @"Nav_MyOils"];
    int ID = ((UIButton*)sender).tag - 5600;
    [[NSNotificationCenter defaultCenter]postNotificationName:stringArr[ID] object:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{///////////////
    NSString* keyWord = [searchText lowercaseString];
    int count1 = [_appDelegate.commonUses count];
    
    NSMutableArray* commonUsesStatus = [[NSMutableArray alloc]init];
    for (int i = 0; i < count1; i++)
    {

        [commonUsesStatus addObject:@"1"];
    }
    
    
    [_mainTableView reloadData];
}
- (void)refreshData
{

/*
     case 0:
     text = _appDelegate.oilNames[[[_appDelegate.oilIndexFavourites allKeys][indexPath.row] intValue]][1];
     break;
     case 1:
     text = _appDelegate.commonUses[[[_appDelegate.commonUsesFavourites allKeys][indexPath.row] intValue] ][2];
     break;
     case 2:
     text = _appDelegate.myBlends[[[_appDelegate.blendsFavourites allKeys][indexPath.row] intValue] ][1];
     break;
     case 3:
     text = _appDelegate.myContacts[[[_appDelegate.contactFavourites allKeys][indexPath.row] intValue] ][0];
*/
    
    _oilNames = [[NSMutableArray alloc]init];
    for (NSArray* c in _appDelegate.oilNames)
         [_oilNames addObject:c[1]];
    
    _commonUses = [[NSMutableArray alloc]init];
    for (NSArray* c in _appDelegate.commonUses)
        [_commonUses addObject:c[2]];
    
    _myBlends = [[NSMutableArray alloc]init];
    for (NSArray* c in _appDelegate.myBlends)
        [_myBlends addObject:c[1]];
    
    _myContacts = [[NSMutableArray alloc]init];
    for (NSArray* c in _appDelegate.myContacts)
        [_myContacts addObject:c[1][0]];
    
    NSLog(@"%@",_appDelegate.oilNotes);
    NSLog(@"%@",_appDelegate.myNotes);
    NSLog(@"%@",_appDelegate.noteFavourites);
    
    
    
    _noteFavourites = [_appDelegate.noteFavourites allKeys];
    NSLog(@"%@",_noteFavourites);
    
    _myNotes = [[NSMutableArray alloc]init];
    for (int i = 0; i < [_noteFavourites count]; i++) {
        
        NSArray *array = [_appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%@",[_noteFavourites objectAtIndex:i]]];
        if (array == nil) {
        } else {
            [_myNotes addObject:array[0]];
        }
    }

    
    NSLog(@"%@",_appDelegate.oilNotes);
    
    
    _oilFavourites = [_appDelegate.oilIndexFavourites allKeys];
    _commonUsesFavourites = [_appDelegate.commonUsesFavourites allKeys];
    _blendsFavourites = [_appDelegate.blendsFavourites allKeys];
    _contactFavourites = [_appDelegate.contactFavourites allKeys];
    
    
    _oilKeys = [[NSMutableArray alloc]init];
    for (int i = 0; i < [_oilFavourites count]; i++)
        [_oilKeys addObject:[NSString stringWithFormat:@"%d", i]];
    
    [self sortKeys:_oilKeys array:_oilFavourites inarray:_oilNames key:1];
    
    _commonUsesKeys = [[NSMutableArray alloc]init];
    for (int i = 0; i < [_commonUsesFavourites count]; i++)
        [_commonUsesKeys addObject:[NSString stringWithFormat:@"%d", i]];
    
    [self sortKeys:_commonUsesKeys array:_commonUsesFavourites inarray:_commonUses key:2];
    
    _blendsKeys= [[NSMutableArray alloc]init];
    for (int i = 0; i < [_blendsFavourites count]; i++)
        [_blendsKeys addObject:[NSString stringWithFormat:@"%d", i]];
    
    [self sortKeys:_blendsKeys array:_blendsFavourites inarray:_myBlends key:1];
    
    _contactKeys = [[NSMutableArray alloc]init];
    for (int i = 0; i < [_contactFavourites count]; i++)
        [_contactKeys addObject:[NSString stringWithFormat:@"%d", i]];
    
    [self sortKeys:_contactKeys array:_contactFavourites inarray:_myContacts key: 0];
    
    _noteKeys = [[NSMutableArray alloc]init];
    for (int i = 0; i < [_noteFavourites count]; i++)
        [_noteKeys addObject:[NSString stringWithFormat:@"%d", i]];
    
    //[self sortKeys:_noteKeys array:_noteFavourites inarray:_myNotes key:0];
    

    
    
    
    
    
}

-(void)sortKeys:(NSMutableArray*)keys
          array:(NSArray*)favourites
        inarray:(NSArray*)names
            key:(int)key
{
    int count = [favourites count];
    
    for (int i = 0; i < count - 1; i++)
        for (int j = i + 1; j < count; j++)
            if ([names[[favourites[[keys[i] intValue]] intValue]] caseInsensitiveCompare:names[[favourites[[keys[j] intValue]] intValue]]] == NSOrderedDescending)
            {
                NSString* temp = keys[i];
                keys[i] = keys[j];
                keys[j] = temp;
            }
}
@end
