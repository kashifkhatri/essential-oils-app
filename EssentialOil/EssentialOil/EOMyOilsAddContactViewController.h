//
//  EOMyOilsAddContactViewController.h
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLSwipeForOptionsCell.h"
#import "EOMyOilsShareListViewController.h"

@interface EOMyOilsAddContactViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,TLSwipeForOptionsCellDelegate>
@property NSString* selectedIndex;
@property UIViewController* parentVCX;

@end
