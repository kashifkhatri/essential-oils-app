//
//  EODidYouKnowDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "EOMyNotesViewController.h"
@interface EOMyNotesDetailViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (nonatomic, strong) NSString *stringTitle;
@property UIViewController* parentVCX;
@property int noteID;
@property BOOL isNoteSharing;
@end

