//
//  EOCommonUseDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 1/7/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOCommonUseDetailViewController.h"
#import "DWTagList.h"
#import "EOMyNotesDetailViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "EOPrecautionViewController.h"
#import "EOOilIndexDetailViewController.h"

@interface EOCommonUseDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *favouriteIcon;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet DWTagList *commonUsesList;
@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UILabel *favouriteLabel;
@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;
@property AppDelegate* appDelegate;
@end

@implementation EOCommonUseDetailViewController
@synthesize appDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    //NSLog(@"%@",_arrUsedOils);
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:appDelegate.oilNames];
    
    arrOilNames = [[NSMutableArray alloc]init];
    
    for (int i =0 ; i < [array count]; i++) {
        NSArray *arrayInner = [array objectAtIndex:i];
        //NSLog(@"%@",[arrayInner objectAtIndex:0]);
        //NSLog(@"%@",[arrayInner objectAtIndex:1]);
        //NSLog(@"===");
        
        if ([_arrUsedOils containsObject:[arrayInner objectAtIndex:1]]) {
            
            NSMutableArray *arrayNameWithID = [[NSMutableArray alloc] init];
            [arrayNameWithID addObject:[arrayInner objectAtIndex:0]];
            [arrayNameWithID addObject:[arrayInner objectAtIndex:1]];
            [arrayNameWithID addObject:[NSNumber numberWithInt:i]];
            [arrOilNames addObject:arrayNameWithID];
        }
    }


    [_commonUsesList setTags:_arrUsedOils];
    
    CGRect rt = _commonUsesList.frame;
    rt.size.height = _commonUsesList.contentSize.height;
    _commonUsesList.frame = rt;

    rt = _subView.frame;
    rt.origin.y = _commonUsesList.frame.origin.y + _commonUsesList.frame.size.height + 10;
    _subView.frame = rt;
    
    _mainScrollView.contentSize = CGSizeMake(_mainScrollView.frame.size.width, MAX(rt.size.height + rt.origin.y, _mainScrollView.frame.size.height) + 40);
    _mainScrollView.contentOffset = CGPointMake(0, 40);
    appDelegate = [[UIApplication sharedApplication] delegate];
    [(UILabel*)[self.view viewWithTag:10] setText:appDelegate.commonUses[_selectedIndex][2]];
    
    if ([appDelegate.commonUsesFavourites objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex]])
    {
        [_favouriteIcon setHidden:NO];
        [_favouriteButton setImage:[UIImage imageNamed:@"ES_hearts.png"] forState:UIControlStateNormal];
        _favouriteLabel.text = @"Favorites";
    }
    else {
        
        [_favouriteIcon setHidden:YES];
        [_favouriteButton setImage:[UIImage imageNamed:@"ES_IconFavorites.png"] forState:UIControlStateNormal];
        _favouriteLabel.text = @"Add to Favorites";
    }
    
    [_commonUsesList setTagBackgroundColor:[UIColor colorWithRed:242.0 / 255 green:156.0 / 255 blue:30.0 / 255 alpha:1.00]];
    
    NSArray* arrays = [_commonUsesList subviews];
    
    for (int i = 0; i < [arrays count]; i++)
    {
        UIView* cView = arrays[i];
        if ([cView isKindOfClass:[DWTagView class]])
        {
            UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onClickedOilUseItem:)];
            recognizer.numberOfTapsRequired = 1;
            cView.userInteractionEnabled = YES;
            NSInteger tag = (int) i;//[[[arrOilNames objectAtIndex:i] objectAtIndex:0] integerValue] + 800;
            cView.tag = tag;
            [cView addGestureRecognizer:recognizer];
            
        }
    }

    [_commonUsesList centerAlign];
    
    id note = ([appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex + 1000]]);
    if (note)
    {
        [_noteIcon setHidden:NO];
    } else {
        [_noteIcon setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    if (scrollView.contentOffset.y <= 0)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onFavourites:(id)sender {
    if (![appDelegate.commonUsesFavourites objectForKey:[NSString stringWithFormat:@"%d", _selectedIndex]])
    {
        [appDelegate.commonUsesFavourites setObject:@"1" forKey:[NSString stringWithFormat:@"%d", _selectedIndex]];
        [_favouriteIcon setHidden:NO];
        [_favouriteButton setImage:[UIImage imageNamed:@"ES_hearts.png"] forState:UIControlStateNormal];
        _favouriteLabel.text = @"Remove Favorite";
        
        [SVProgressHUD showSuccessWithStatus:@"Added to Favorites"];
    }
    else {

        [appDelegate.commonUsesFavourites removeObjectForKey:[NSString stringWithFormat:@"%d", _selectedIndex]];
        [_favouriteIcon setHidden:YES];
        [_favouriteButton setImage:[UIImage imageNamed:@"ES_IconFavorites.png"] forState:UIControlStateNormal];
        _favouriteLabel.text = @"Add to Favorites";
        
        [SVProgressHUD showSuccessWithStatus:@"Removed From Favorites"];
    }
    
    
    
    [[NSUserDefaults standardUserDefaults]setObject:appDelegate.commonUsesFavourites forKey:@"_commonUsesFavourites"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (IBAction)onHowToUse:(id)sender {
    EOPrecautionViewController * viewController = (EOPrecautionViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"precaution"];
    viewController.mode = 2;
    viewController.precautionIDs = [@"1,2,3,4,5" componentsSeparatedByString:@","];
    [self presentViewController:viewController animated:YES completion:nil];
}
-(IBAction)onAddNotes:(id)sender
{
    EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
    
    viewController.noteID = _selectedIndex + 1000;
    viewController.parentVCX = self;
    viewController.stringTitle = appDelegate.commonUses[_selectedIndex][2];
    [self presentViewController:viewController animated:YES completion:nil];
}

-(void)onClickedOilUseItem:(UITapGestureRecognizer*)recog
{
    UILabel* sender = (UILabel*)(recog.view);
    EOOilIndexDetailViewController* viewController = (EOOilIndexDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"oilindexdetail"];
    
    NSInteger rowIndex = ((UILabel*)sender).tag;
    //((UILabel*)sender).tag - 800;
    
    NSInteger oilIndex = [[[arrOilNames objectAtIndex:rowIndex] objectAtIndex:2] integerValue];
    
    NSLog(@"%ld",(long)rowIndex);
    
    
    
    viewController.selectedIndex = (int)oilIndex;
    [self presentViewController:viewController animated:YES completion:nil];
    
    /*
    viewController.selectedIndex = rowIndex;
    int oilCount = [appDelegate.oilNames count];
    
    
    NSString* temp;
    NSArray* tempArr;
    NSMutableArray* usedArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < oilCount; i++)
    {
        temp = appDelegate.oilNames[i][5];
        
        tempArr = [temp componentsSeparatedByString:@","];
        int flag = 0;
        for (NSString* c in tempArr)
        {
            int p = [c intValue];
            if (p < 1) continue;
            
            if(rowIndex == p)
            {
                flag = 1; break;
            }
        }
        
        if (flag)
            [usedArr addObject:appDelegate.oilNames[i][1]];
    }
    
    viewController.arrUsedOils = [[NSArray alloc]initWithArray:usedArr];
    [self presentViewController:viewController animated:YES completion:nil];
     */
}
@end
