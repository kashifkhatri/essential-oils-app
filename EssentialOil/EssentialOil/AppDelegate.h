//
//  AppDelegate.h
//  EssentialOil
//
//  Created by Steve on 1/4/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#define IMAGE_SIZE 25
#define CURRENT_VERSION 1.0
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray* commonUses, *generalInformation, *howToUse, *preCaution, *tips, *oilNames, *myBlends, *myContacts, *myNotes;

@property (strong, nonatomic) NSMutableDictionary* oilNotes, *myOils, *myOilsShare;

@property (strong, nonatomic) NSMutableDictionary* oilIndexFavourites   , *commonUsesFavourites, *blendsFavourites, *contactFavourites, *noteFavourites;

-(void)saveData;

- (void) saveOilData:(NSMutableDictionary *) dictionary;

@end

