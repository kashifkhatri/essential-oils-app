//
//  EOMyOilsShareListViewController.m
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "AppDelegate.h"
#import "TLSwipeForOptionsCell.h"
#import "EOMyOilsShareListViewController.h"
#import "EOMyOilsViewController.h"
#import "EOMyOilsShareDetailViewController.h"
#import "EOMyOilsAddContactViewController.h"
#import "EOMyOilsShareViewController.h"
#import "SVProgressHUD.h"


@interface EOMyOilsShareListViewController()
@property (weak, nonatomic) IBOutlet UILabel *oilNameText;
@property (weak, nonatomic) IBOutlet UILabel *stockCount;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITableView *contactTableView;
@property AppDelegate* appDelegate;
@property NSMutableDictionary* dataDict, *myOil;
@property NSArray* dataKeys, *dataValues;
@end
@implementation EOMyOilsShareListViewController

- (IBAction)onBack:(id)sender {
    [self syncMyOils];
    [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOils forKey:@"_myOils"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHome:(id)sender {
    [self syncMyOils];
    [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOils forKey:@"_myOils"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}

- (void) syncMyOils
{
    NSLog(@"%@", _appDelegate.myOils);
    if ([_appDelegate.myOilsShare objectForKey:_selectedIndex]) {
        
    } else if ([[_stockCount text] intValue] == 0) {
        NSLog(@"%@", [_appDelegate.myOils objectForKey:_selectedIndex]);
        //[_appDelegate.myOils removeObjectForKey:_selectedIndex];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    _appDelegate = [[UIApplication sharedApplication]delegate];
    NSString* text;
    NSInteger stockCount = 0;
    
    // if (index > 299990)
    
    NSLog(@"%@",_appDelegate.myBlends);
    
    if ([_selectedIndex hasPrefix:@"N_"])
    {
        NSLog(@"%@",[_appDelegate.myOils objectForKey:_selectedIndex]);
        text = [_appDelegate.myOils objectForKey:_selectedIndex][1];
        stockCount = [[NSString stringWithFormat:@"%@",[_appDelegate.myOils objectForKey:_selectedIndex][0]] integerValue];
    }
    else {
        int index = [_selectedIndex intValue];
        if (index > 299000)
        {
            NSInteger finalIndex = index - 300000;
            //text = _appDelegate.myBlends[-finalIndex - 1][1];
            
            NSArray *array = _appDelegate.myBlends[-finalIndex - 1];
            text = array[1];
            
             stockCount = [[NSString stringWithFormat:@"%02d", [[_appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%ld",(long)finalIndex]][0] intValue]] integerValue];
            
//            for (int i = 0; i < [array[2] count]; i++)
//            {
//                stockCount += [array[2][i][1] integerValue];
//            }
            
            _selectedIndex = [NSString stringWithFormat:@"%ld",(long)finalIndex];
            
        }
        else if (index < 0) {

            NSArray *array = _appDelegate.myBlends[-index - 1];
            text = array[1];
            
            stockCount = [[NSString stringWithFormat:@"%02d", [[_appDelegate.myOils objectForKey:[NSString stringWithFormat:@"%ld",(long)index]][0] intValue]] integerValue];
            
//            for (int i = 0; i < [array[2] count]; i++)
//            {
//                stockCount += [array[2][i][1] integerValue];
//            }
        }
        else
        {
            text = _appDelegate.oilNames[index - 1][1];
            stockCount = [[NSString stringWithFormat:@"%02d", [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue]] integerValue];
        }
//        if (index > 0)
//        else
    }
    
    [_oilNameText setText:text];
    [_stockCount setText:[NSString stringWithFormat:@"%02ld",(long)stockCount]];
    
    [self reloadTableData];
    
    _myOil = [[NSMutableDictionary alloc] initWithDictionary:_appDelegate.myOils];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _appDelegate = [[UIApplication sharedApplication]delegate];
    
    [self reloadTableData];
}

-(void)reloadTableData
{
    _appDelegate = [[UIApplication sharedApplication]delegate];
    _dataDict = [[NSMutableDictionary alloc]initWithDictionary:[_appDelegate.myOilsShare objectForKey:_selectedIndex]];
    
    NSLog(@"%@",_appDelegate.myContacts);
    NSLog(@"%@",_dataDict);
    
    CGRect rt = _contactTableView.frame;
    rt.size.height = 35 * [[_dataDict allKeys] count];
    [_contactTableView setFrame:rt];
    _mainScrollView.contentSize = CGSizeMake(320, rt.size.height + rt.origin.y + 50);
    _dataKeys = [_dataDict allKeys];
    _dataValues = [_dataDict allValues];
    
    [_contactTableView reloadData];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_dataDict allKeys]count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //    cell.buttonCount = 0;
    //    cell.textLabel.text = [NSString stringWithFormat:@"%d. %@", indexPath.row + 1, appDelegate.tips[indexPath.row][1]];
    //    cell.delegate = self;
    //    cell.indexPath = indexPath;
    //    cell.scrollView.contentSize = cell.frame.size;
    //    cell.scrollViewButtonView.frame = CGRectMake(0,0,0,0);
    //    return cell;
    
    
    [cell.scrollView removeFromSuperview];
    [[cell.contentView viewWithTag:2003]removeFromSuperview];
    UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 280, 49)];
    label.tag = 2003;
    int count = [_dataValues[indexPath.row][0] intValue];
    
    NSString* unit;
    
    if ([_dataValues[indexPath.row][1] intValue] == 0) unit = @"Drop";
    else unit = @"Bottle";
    
    if (count > 1) unit = [unit stringByAppendingString:@"s"];

    int indexX = [_dataKeys[indexPath.row] intValue];
    //NSUInteger objectIndex = [_dataKeys indexOfObject:[NSString stringWithFormat:@"%d",indexX]];
    
    [label setText:[NSString stringWithFormat:@"%@ has %02d %@", [_appDelegate.myContacts objectAtIndex:indexX][1][0], count, unit]];
    [cell.contentView addSubview:label];
    cell.delegate = self;
    cell.indexPath = indexPath;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* arr = @[_dataValues[indexPath.row][0], _dataValues[indexPath.row][1], _appDelegate.myContacts[[_dataKeys[indexPath.row] intValue]][1][0], _dataValues[indexPath.row][2]];
    
    EOMyOilsShareDetailViewController* vc = (EOMyOilsShareDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilssharedetails"];
    
    vc.dataArr = arr;
    vc.indexPathRow = indexPath.row;
    vc.selectedIndex = _selectedIndex;
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)onPlus:(id)sender {
    
    
    int c = [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue];
    NSLog(@"%@", _appDelegate.myOils);
    
    NSLog(@"%@", [_appDelegate.myOils objectForKey:_selectedIndex]);
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[_appDelegate.myOils objectForKey:_selectedIndex]];
    [array replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:c + 1]];

    [_appDelegate.myOils setObject:array forKey:_selectedIndex];
    
    [_stockCount setText:[NSString stringWithFormat:@"%02d", [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue]]];
    
}

- (IBAction)onMInus:(id)sender {
    
    int c = [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue];
    if (c == 0) return;
    NSLog(@"%@", _appDelegate.myOils);
    
    NSLog(@"%@", [_appDelegate.myOils objectForKey:_selectedIndex]);
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[_appDelegate.myOils objectForKey:_selectedIndex]];
    [array replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:c - 1]];
    
    [_appDelegate.myOils setObject:array forKey:_selectedIndex];
    
    [_stockCount setText:[NSString stringWithFormat:@"%02d", [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue]]];
    /*
    int c = [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue];
    if (c == 0) return;
    [_appDelegate.myOils objectForKey:_selectedIndex][0] = [NSNumber numberWithInt:c - 1];
    [_stockCount setText:[NSString stringWithFormat:@"%02d", [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue]]];
     */
    
}

- (IBAction)onAddNewContact:(id)sender {
    
    int c = [[_appDelegate.myOils objectForKey:_selectedIndex][0] intValue];
    
    if (c < 1) {
        [SVProgressHUD showSuccessWithStatus:@"You don't have that many oils to share"];
    } else {
        EOMyOilsShareViewController* vc = (EOMyOilsShareViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsshare"];
        vc.selectedIndex = _selectedIndex;
        [self.navigationController pushViewController:vc animated:YES];
        
    //    EOMyOilsAddContactViewController* vc = (EOMyOilsAddContactViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsaddcontact"];
    //    vc.parentVCX = self;
    //    vc.selectedIndex = _selectedIndex;
    //    [self.navigationController pushViewController:vc animated:YES];
    }
    
}

-(void)contactAdded:(id)sender
{
    [self reloadTableData];
}

@end
