//
//  EOSettingsViewController.m
//  EssentialOil
//
//  Created by Steve on 2/24/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOSettingsViewController.h"
#import "AppDelegate.h"

@interface EOSettingsViewController()
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView1;
@property (weak, nonatomic) IBOutlet UIWebView *webView2;
@property (weak, nonatomic) IBOutlet UIWebView *webView3;
@property AppDelegate* appDelegate;
@property NSArray* keys;
@end

@implementation EOSettingsViewController
- (IBAction)onHome:(id)sender {
    
    if ([_mainTableView isHidden]) {
        [_mainTableView setHidden:NO];
        [[self.view viewWithTag:2 * 10 + 10] setHidden:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    

}

-(void)viewDidLoad
{
    //@"Share This App", @"Rate This App",
    _keys = @[@"Disclaimer", @"About JellyBeez", @"WalkThrough", @"More Apps",  @"Contact Us", @"Clear All Notes", @"Clear All Contacts", @"Reset Hidden Items", @"Reset this App"];
    
    [super viewDidLoad];
    _appDelegate = [[UIApplication sharedApplication]delegate];
    _scrollView1.contentSize = CGSizeMake(320, 518);
    
    
    NSString* htmlString = @"JellyBeez! is a Columbus, Ohio-based app publisher. Founded by graphic artist Christopher MeaD, JellyBeez!is committed to creating quality imaginative educational experiences that help everyone from toddlers to adults help better the world around them. To learn more about JellyBeez! Feel free to connect and learn more by clicking any of the links below.<br> \
        <a href = 'https://www.facebook.com/jellybeezapps'><img src = 'facebook.png'></a><a href = 'https://plus.google.com/+jellybeezapps'><img src = 'google_plus.png'></a><a href = 'http://twitter.com/jellybeezapps'><img src = 'twitter.png'><a href = 'http://www.youtube.com/user/jellybeezapps'><img src = 'youtube.png'></a><a href = 'http://www.pinterest.com/jellybeezapps'><img src = 'pinterest.png'></a>";
    
    [_webView2 loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    
    [_webView3 loadHTMLString:@"<img src = 'Walkthough_Image.jpg'>" baseURL:[[NSBundle mainBundle] bundleURL]];
    [_webView3 setScalesPageToFit:YES];
    
    //[_webView3 loadHTMLString:@"Click the word and goes to the app store link below: <br>\
    // <a href = 'https://itunes.apple.com/us/artist/jellybeez!-llc./id905626239'>https://itunes.apple.com/us/ //artist// / jellybeez!-llc./id905626239</a>" baseURL:nil];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_keys count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    
    for (UIView* c in cell.subviews)
         [c removeFromSuperview];
    
    UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 320, 35)];
    [label setText:_keys[indexPath.row]];
    [cell addSubview:label];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
    switch(indexPath.row)
    {
        case 0:
        case 1:
        case 2:
        {
            [_mainTableView setHidden:YES];
            [[self.view viewWithTag:indexPath.row * 10 + 10] setHidden:NO];
            break;
        }
            
        case 3:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/artist/jellybeez!-llc./id905626239"]];
             // Rate app
            break;
        /*
        case 4: //Contact us
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                mail.mailComposeDelegate = self;
                [mail setSubject:@"Contact Us"];
                [mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];
                [mail setToRecipients:@[@"jellybeezapps@gmail.com"]];
                
                [self presentViewController:mail animated:YES completion:NULL];
            }
            else
            {
                NSLog(@"This device cannot send email");
            }
        }
            break;
         */
        case 4:
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://jellybeezapps.com"]];
            break;
        }
        case 5:
        {
            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            alertView.tag = 100 + indexPath.row * 2 - 10;
            [alertView show];
            break;
        }
        case 6:
        {
            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            alertView.tag = 100 + indexPath.row * 2 - 10;
            [alertView show];
            break;
        }
        case 7:
        {
            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            alertView.tag = 100 + indexPath.row * 2 - 6;
            [alertView show];
            break;
        }
//            [[[UIAlertView alloc]initWithTitle:@"Sign-Up for Emails" message:@"What is the best way to collect emails?" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
//            break;
        case 8:
        {
            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            alertView.tag = 100 + indexPath.row * 2 - 6;
            [alertView show];
            break;
        }
        case 9:
        case 10:
        case 11:
        {
            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            alertView.tag = 100 + indexPath.row * 2 - 12;
            [alertView show];
            break;
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) return;
    
    if (alertView.tag >= 100 && alertView.tag %2 == 0)
    {
        UIAlertView* alertView1 = [[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure this is permanent?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView1.tag = alertView.tag + 1;
        [alertView1 show];
        return;
    }
    NSLog(@"%d", alertView.tag);
    switch (alertView.tag)
    {
        case 101:
        {
            [_appDelegate.oilNotes removeAllObjects];
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.oilNotes forKey:@"_oilNotes"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
            break;
        case 103:
        {
            [_appDelegate.myContacts removeAllObjects];
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myContacts forKey:@"_myContacts"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
            break;
            
        case 109:
        {
            for (int i = 0; i < [_appDelegate.oilNames count]; i++)
            {
                NSMutableArray*c = [[NSMutableArray alloc]initWithArray: _appDelegate.oilNames[i] ];
                if ([c count] > 7)     [c removeObjectAtIndex:7];
                [_appDelegate.oilNames replaceObjectAtIndex:i withObject:c];

            }
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.oilNames forKey:@"oilNames"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            break;
        }
        case 107:
        {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"oilNames"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"generalInformation"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"commonUses"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"tips"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"preCaution"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"howToUse"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_oilIndexFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_blendsFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_commonUsesFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_oilNotes"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myOils"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myBlends"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myContacts"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_contactFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_noteFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myOilsShare"];
            
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [_appDelegate application:nil didFinishLaunchingWithOptions:nil];
            break;
        }
            
        case 111:
        {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"oilNames"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"generalInformation"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"commonUses"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"tips"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"preCaution"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"howToUse"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_oilIndexFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_blendsFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_commonUsesFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_oilNotes"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myOils"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myBlends"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myContacts"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_contactFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_noteFavourites"];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_myOilsShare"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"FirstTime"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"version"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [_appDelegate application:nil didFinishLaunchingWithOptions:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
