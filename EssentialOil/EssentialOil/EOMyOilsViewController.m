//
//  EOMyOilsViewController.m
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyOilsViewController.h"

@interface EOMyOilsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;

@end

@implementation EOMyOilsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onBack:)];
    recognizer.numberOfTapsRequired = 1;
    [_titleImageView addGestureRecognizer:recognizer];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onMyOils:) name:@"menu_myoils" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onMyNotes:) name:@"menu_mynotes" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onMyBlends:) name:@"menu_myblends" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onMyContacts:) name:@"menu_mycontacts" object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onMyOils:(id)sender {
    [self.navigationController popToViewController:self animated:NO];
    UIViewController* vC = [self.storyboard instantiateViewControllerWithIdentifier:@"myoilsvc"];
    [self.navigationController pushViewController:vC animated:YES];
}
- (IBAction)onMyBlends:(id)sender {
    [self.navigationController popToViewController:self animated:NO];
    UIViewController* vC = [self.storyboard instantiateViewControllerWithIdentifier:@"myblendsview"];
    [self.navigationController pushViewController:vC animated:YES];
    
}
- (IBAction)onMyNotes:(id)sender {
    [self.navigationController popToViewController:self animated:NO];
    UIViewController* vC = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesview"];
    [self.navigationController pushViewController:vC animated:YES];
    
}
- (IBAction)onMyContacts:(id)sender {
    [self.navigationController popToViewController:self animated:NO];
    UIViewController* vC = [self.storyboard instantiateViewControllerWithIdentifier:@"mycontactsview"];
    [self.navigationController pushViewController:vC animated:YES];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
