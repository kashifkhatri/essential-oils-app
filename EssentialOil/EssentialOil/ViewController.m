//
//  ViewController.m
//  EssentialOil
//
//  Created by Steve on 1/4/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UILabel *contentDisclaimer;
@property (weak, nonatomic) IBOutlet UILabel *contentWelcome;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIScrollView *disclaimerScroll;
@property int step;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _step = 0;
    [_disclaimerScroll setContentSize:CGSizeMake(320, 568)];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* stringData = [[NSUserDefaults standardUserDefaults]objectForKey:@"FirstTime"];
    
    NSString* versionData = [[NSUserDefaults standardUserDefaults]objectForKey:@"version"];
    
    float verNum = (versionData == nil ? 0 : [versionData floatValue]);
    
    if (verNum >= CURRENT_VERSION || [@"No" isEqualToString:stringData])
    {
        self.view.hidden = YES;
        UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuview"];
        [self.navigationController pushViewController:viewController animated:NO];

        return;
    } else {
        self.view.hidden = NO;
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.view.hidden = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onNextButtonClicked:(id)sender {
    
    if (_step) // To Next Screen
    {
        
        NSString* stringData = [[NSUserDefaults standardUserDefaults]objectForKey:@"FirstTime"];
        
        NSString* versionData = [[NSUserDefaults standardUserDefaults]objectForKey:@"version"];
        
        float verNum = (versionData == nil ? 0 : [versionData floatValue]);
        
        if (verNum >= CURRENT_VERSION || [@"No" isEqualToString:stringData])
        {
            UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuview"];
            [self.navigationController pushViewController:viewController animated:YES];
            
            return;
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"FirstTime"];
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%0.00f", CURRENT_VERSION] forKey:@"version"];
        
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"welcomeview"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else //To Welcome Screen
    {
        [_titleText setText:@"Welcome"];
        [_nextButton setTitle:@"Welcome!" forState:UIControlStateNormal];
        
        [_contentDisclaimer setHidden:YES];
        [_contentWelcome setHidden:NO];
        [_disclaimerScroll setContentOffset:CGPointMake(0, 0) animated:YES];
        [_disclaimerScroll setHidden:NO];
        _step = 1;
        
    }
}

@end
