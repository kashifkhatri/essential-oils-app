//
//  EOMyOilsShareListViewController.h
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLSwipeForOptionsCell.h"

@interface EOMyOilsShareListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, TLSwipeForOptionsCellDelegate>
@property NSString* selectedIndex;
@property BOOL isBlend;
-(void)contactAdded:(id)sender;
@end
