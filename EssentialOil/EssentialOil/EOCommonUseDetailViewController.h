//
//  EOCommonUseDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 1/7/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOCommonUseDetailViewController : UIViewController<UIScrollViewDelegate>
{
    NSMutableArray* arrOilNames;
}

@property (weak, nonatomic) IBOutlet UIImageView *noteIcon;

@property NSArray* arrUsedOils;
@property int selectedIndex;

@end
