//
//  EOPrecautionViewController.h
//  EssentialOil
//
//  Created by Steve on 2/16/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOPrecautionViewController : UIViewController<UIScrollViewDelegate>
@property int mode;
@property NSArray* precautionIDs;
@end
