//
//  EOMyOilsAddNewOilViewController.h
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOMyOilsAddNewOilViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property UIViewController* parentVCX;
@end
