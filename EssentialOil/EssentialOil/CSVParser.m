//
//  CSVParser.m
//  CSVParser
//
//  Created by XinZhangZhe on 4/4/13.
//  Copyright (c) 2013 Xin ZhangZhe. All rights reserved.
//
// --- Headers --- ;
#import "CSVParser.h"

// --- Defines --- ;
// CSVParser Class ;
@interface CSVParser ()
{
    NSScanner*      scanner ;
	NSString*       csvString ;
	NSString*       separator ;
	BOOL            separatorIsSingleChar ;
	NSCharacterSet* characterSetForEndText ;
}

// Functions ;
- ( NSArray* ) parseLine ;
- ( NSString* ) parseName ;
- ( NSString* ) parseField ;
- ( NSString* ) parseEscaped ;
- ( NSString* ) parseNonEscaped ;
- ( NSString* ) parseDoubleQuote ;
- ( NSString* ) parseSeparator ;
- ( NSString* ) parseLineSeparator ;
- ( NSString* ) parseTwoDoubleQuotes ;
- ( NSString* ) parseTextData ;

@end 

@implementation CSVParser

#pragma mark - CSVParser
- ( id ) initWithString : ( NSString* ) _csvString
              separator : ( NSString* ) _separator
{
	self = [super init];
	if (self)
	{
		csvString = [ _csvString retain ] ;
		separator = [ _separator retain ] ;
		
		NSAssert( [ separator length ] > 0 &&
                  [ separator rangeOfString : @"\""].location == NSNotFound &&
                  [ separator rangeOfCharacterFromSet : [ NSCharacterSet newlineCharacterSet ] ].location == NSNotFound,
                  @"CSV separator string must not be empty and must not contain the double quote character or newline characters." ) ;
		
		NSMutableCharacterSet*  characterSetForEndMutableText = [ [ NSCharacterSet newlineCharacterSet ] mutableCopy ] ;
        
		[ characterSetForEndMutableText addCharactersInString : @"\"" ] ;
		[ characterSetForEndMutableText addCharactersInString : [ separator substringToIndex : 1 ] ] ;
        
		characterSetForEndText = characterSetForEndMutableText ;
        
		if( [ separator length ] == 1 )
		{
			separatorIsSingleChar = YES ;
		}
    }
	
	return self;
}

- ( void ) dealloc
{
    // Release ;
	[ csvString release ] ;
	[ separator release ] ;
	[ characterSetForEndText release ] ;
    
	[ super dealloc ] ;
}

- ( NSArray* ) Parse
{
	NSMutableArray* arrForLines = [ NSMutableArray array ] ; ;

    // Scanner ;
    scanner = [ [ NSScanner alloc ] initWithString : csvString ] ;
    
    // Skipped Characters ;
	[ scanner setCharactersToBeSkipped : [ [ [ NSCharacterSet alloc ] init ] autorelease ] ] ;

	// Parse ;
    do {
        // Pool ;
		NSAutoreleasePool*  pool = [ [ NSAutoreleasePool alloc ] init ] ;
        
        // Line ;
        NSArray*    arrForTokens    = [ self parseLine ] ;
        
        if( arrForTokens == nil )
        {
            [ pool drain ] ;
            break ;
        }
        
        [ arrForLines addObject : arrForTokens ] ;
        
        if( ![ self parseLineSeparator ] )
		{
            [ pool drain ] ;
			break ;
		}
        
        [ pool drain ] ;
        
    } while( YES ) ;

	// Release ;
	[ scanner release ] ;
	scanner = nil ;
	
    // Return ;
	return arrForLines ;
}

- ( NSArray* ) parseLine
{
	if( [ self parseLineSeparator ] || [ scanner isAtEnd ] )
	{
		return nil ;
	}
	
	NSString*   field = [ self parseField ] ;
    
	if( !field )
	{
		return nil ;
	}
    
    NSMutableArray* arryForToken    = [ NSMutableArray array ] ;

	while( field )
	{
		[ arryForToken addObject : field ] ;
        
		if( ![ self parseSeparator ] )
		{
			break ;
		}
		
		field = [ self parseField ] ;
	}
	
	return arryForToken ;
}

- ( NSString* ) parseName
{
	return [ self parseField ] ;
}

- ( NSString* ) parseField
{
    // Escaped ;
	NSString*   strForEscaped       = [ self parseEscaped ] ;
    
	if( strForEscaped )
	{
		return strForEscaped ;
	}
	
    // Non Escaped ;
	NSString*   strForNonEscaped    = [ self parseNonEscaped ] ;
    
	if( strForNonEscaped )
 	{
		return strForNonEscaped ;
	}
	
    // Location ;
	NSInteger   location            = [ scanner scanLocation ] ;
    
	if( [ self parseSeparator ] || [ self parseLineSeparator ] || [ scanner isAtEnd ] )
	{
		[ scanner setScanLocation : location ] ;
		return @"" ;
	}
    
	return nil ;
}

- ( NSString* ) parseEscaped
{
    // Double Quote ;
	if( ![ self parseDoubleQuote ] )
	{
		return nil ;
	}
	
	NSString* accumulatedData = [ NSString string ] ;
    
	while( YES )
	{
		NSString* fragment = [ self parseTextData ] ;
        
		if( !fragment )
		{
			fragment = [ self parseSeparator ] ;
            
			if( !fragment )
			{
				fragment = [ self parseLineSeparator ] ;

				if( !fragment )
				{
					if( [ self parseTwoDoubleQuotes ] )
					{
						fragment = @"\"" ;
					}
					else
					{
						break ;
					}
				}
			}
		}
		
		accumulatedData = [ accumulatedData stringByAppendingString : fragment ] ;
	}
	
	if( ![ self parseDoubleQuote ] )
	{
		return nil ;
	}
	
	return accumulatedData ;
}

- ( NSString* ) parseNonEscaped
{
	return [ self parseTextData ] ;
}

- ( NSString* ) parseTwoDoubleQuotes
{
	if( [ scanner scanString : @"\"\"" intoString : NULL ] )
	{
		return @"\"\"" ;
	}
    
	return nil ;
}

- ( NSString* ) parseDoubleQuote
{
	if( [ scanner scanString : @"\"" intoString : NULL ] )
	{
		return @"\"" ;
	}
    
	return nil ;
}

- ( NSString* ) parseSeparator
{
	if( [ scanner scanString : separator intoString : NULL ] )
	{
		return separator ;
	}
    
	return nil ;
}

- ( NSString* ) parseLineSeparator
{
	NSString*   matchedNewlines = nil ;
    
	[ scanner scanCharactersFromSet : [ NSCharacterSet newlineCharacterSet ] intoString : &matchedNewlines ] ;
	return matchedNewlines ;
}

- ( NSString* ) parseTextData
{
	NSString*   accumulatedData = [ NSString string ] ;
    
	while( YES )
	{
		NSString*   fragment = nil ;

		if( [ scanner scanUpToCharactersFromSet : characterSetForEndText intoString : &fragment ] )
		{
			accumulatedData = [ accumulatedData stringByAppendingString : fragment ] ;
		}
		
		if( separatorIsSingleChar )
		{
			break ;
		}
		
		NSUInteger  location = [ scanner scanLocation ] ;
		NSString*   firstCharOfSeparator ;
        
		if( [ scanner scanString : [ separator substringToIndex : 1 ] intoString : &firstCharOfSeparator ] )
		{
			if( [ scanner scanString : [ separator substringFromIndex : 1 ] intoString : NULL ] )
			{
				[ scanner setScanLocation : location ] ;
				break ;
			}
			
			accumulatedData = [ accumulatedData stringByAppendingString : firstCharOfSeparator ] ;
			continue ;
		}
		else
		{
			break ;
		}
	}
	
	if( [ accumulatedData length ] > 0 )
	{
		return accumulatedData ;
	}
	
	return nil ;
}

@end
