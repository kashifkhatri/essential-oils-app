//
//  EOOilIndexDetailViewController.h
//  EssentialOil
//
//  Created by Steve on 2/16/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EOOilIndexDetailViewController : UIViewController<UIScrollViewDelegate>
@property int selectedIndex;
@property (weak, nonatomic) IBOutlet UIImageView *noteIcon;
- (IBAction)onHome:(id)sender;
@end
