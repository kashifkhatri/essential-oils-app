//
//  EODidYouKnowDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EOOilIndexViewController.h"
#import "EOMyNotesDetailViewController.h"
#import "EOMyNotesViewController.h"
#import "EOOilIndexDetailViewController.h"
#import "SVProgressHUD.h"
#import "EOCommonUseViewController.h"
#import "EOMyOilsViewController.h"
#import "EOMenuViewController.h"
#import "EOMyOilsMainSharingViewController.h"

@interface EOMyNotesDetailViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property AppDelegate* appDelegate;
@end

@implementation EOMyNotesDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _appDelegate = [[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    
    if ([_appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", _noteID]])
    {
        NSArray* arr = [_appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", _noteID]];
        
        [_contentLabel setText:arr[1]];
        [_titleLabel setText:arr[0]];
        
        [_addButton setImage:[UIImage imageNamed:@"ES_ButtonEdit.png"] forState:UIControlStateNormal];
    }
    
    UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTap:)];
    recognizer.numberOfTapsRequired = 1;
    
    [_mainScrollView addGestureRecognizer:recognizer];
    
    if ([_stringTitle length] > 0) {
        _titleLabel.text = [NSString stringWithFormat:@"%@",_stringTitle];
    }
    
    if (_isNoteSharing) {
        [_addButton setTitle:@"Add" forState:UIControlStateNormal];
    }
}
-(void)onTap:(id)sender
{
    _mainScrollView.contentSize = _mainScrollView.frame.size;
    [_contentLabel resignFirstResponder];
    [_titleLabel resignFirstResponder];
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textField
{
    if ([textField.text isEqualToString:[NSString stringWithFormat:@"Enter the Note Here"]]) {
        [textField setText:@""];
    }
    _mainScrollView.contentSize = CGSizeMake(_mainScrollView.frame.size.width, _mainScrollView.frame.size.height + 200);
    return  YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _mainScrollView.contentSize = _mainScrollView.frame.size;
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        [textView setText:[NSString stringWithFormat:@"Enter the Note Here"]];
    }
    _mainScrollView.contentSize = _mainScrollView.frame.size;
    [textView resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onHome:(id)sender {
    if (_isNoteSharing) {
        
        NSArray* arrViewControllers = [self.navigationController viewControllers];
        
        for (int i = 0; [arrViewControllers count] >= i; i++)
        {
            if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
            {
                [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
                break;
            }
        }
        
        //[self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_parentVCX isKindOfClass:[EOOilIndexDetailViewController class]])
        {
            [(EOOilIndexDetailViewController*)_parentVCX onHome:nil];
        }
        else if ([_parentVCX isKindOfClass:[EOMyNotesViewController class]])
        {
            [(EOMyNotesViewController*)_parentVCX onHome:nil];
        }
        else if ([_parentVCX isKindOfClass:[EOCommonUseViewController class]])
        {
            [(EOCommonUseViewController*)_parentVCX onHome:nil];
        }
        else if ([_parentVCX isKindOfClass:[EOMyNotesViewController class]])
        {
            [(EOMyNotesViewController*)_parentVCX onHome:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Nav_Home" object:nil];
        }
    }];
}
- (IBAction)onBack:(id)sender {
    if (_isNoteSharing) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onAddEdit:(id)sender {
    
    if ([_titleLabel.text length] == 0 || [[_titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No Note Title?!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }
    
    if (_isNoteSharing) {
        
        NSArray *array = [[NSArray alloc] initWithObjects:_titleLabel.text, _contentLabel.text, nil];
        [(EOMyOilsMainSharingViewController*)_parentVCX noteSaved:array];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    else if ([_appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", _noteID]])
    {
        [SVProgressHUD showWithStatus:@"Updating"];
        [_appDelegate.oilNotes setObject:@[_titleLabel.text, _contentLabel.text] forKey:[NSString stringWithFormat:@"%d", _noteID]];
        
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.oilNotes forKey:@"_oilNotes"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [_addButton setImage:[UIImage imageNamed:@"ES_ButtonEdit.png"] forState:UIControlStateNormal];
        [UIView animateWithDuration:1 animations:^{
            
        } completion:^(BOOL finished) {
           [SVProgressHUD showSuccessWithStatus:@"Done"];
        }];
    }
    else
    {
        if (_noteID == -1)
        {
            NSMutableArray *_myNotes = [[NSMutableArray alloc] init];
            
            NSArray* arrKeys = [_appDelegate.oilNotes allKeys];
            for (int i = 0; i < [arrKeys count]; i++) {
                if ([[arrKeys objectAtIndex:i] integerValue] >=0 && [[arrKeys objectAtIndex:i] integerValue] < 500 ) {
                    [_myNotes addObject:[arrKeys objectAtIndex:i]];
                }
            }
            _noteID = (int)[_myNotes count];
            /*
            NSArray* arrKeys = [_appDelegate.oilNotes allKeys];
            int maxKeyValue = 10000;
            for (id c in arrKeys)
            {
                int num = [c intValue];
                if (num > maxKeyValue) maxKeyValue = num;
            }
            
            maxKeyValue++;
            
            _noteID = maxKeyValue;
             */
        }
        
        [SVProgressHUD showWithStatus:@"Adding"];
        [_appDelegate.oilNotes setObject:@[_titleLabel.text, _contentLabel.text] forKey:[NSString stringWithFormat:@"%d", _noteID]];
        
        [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.oilNotes forKey:@"_oilNotes"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [_addButton setImage:[UIImage imageNamed:@"ES_ButtonEdit.png"] forState:UIControlStateNormal];
        [UIView animateWithDuration:1 animations:^{
            
        } completion:^(BOOL finished) {
            [SVProgressHUD showSuccessWithStatus:@"Done"];
        }];
        
        NSLog(@"%@",_appDelegate.oilNotes);
    
    }
}
- (IBAction)onDelete:(id)sender {
    [[[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"Are you sure to delete this note?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil]show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (_isNoteSharing) {
            
            NSArray *array = [[NSArray alloc] initWithArray:nil];
            [(EOMyOilsMainSharingViewController*)_parentVCX noteSaved:array];
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            [_appDelegate.oilNotes removeObjectForKey:[NSString stringWithFormat:@"%d", _noteID]];
            
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.oilNotes forKey:@"_oilNotes"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
