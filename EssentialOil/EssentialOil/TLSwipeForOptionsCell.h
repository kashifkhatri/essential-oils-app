//
//  TLSwipeForOptionsCell.h
//  UITableViewCell-Swipe-for-Options
//
//  Created by Ash Furrow on 2013-07-29.
//  Copyright (c) 2013 Teehan+Lax. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLSwipeForOptionsCell;
#define TLSwipeForOptionsCellEnclosingTableViewDidBeginScrollingNotification  @"TLSwipeForOptionsCellEnclosingTableViewDidScrollNotification"

#define kCatchWidth 245 / 5 * _buttonCount
#define CGPointOriginal CGPointMake(kCatchWidth, 0)

@protocol TLSwipeForOptionsCellDelegate <NSObject>

-(void)cellDidSelectDelete:(TLSwipeForOptionsCell *)cell;
-(void)cellDidSelectMore:(TLSwipeForOptionsCell *)cell;
-(void)onSelectCell:(UITableViewCell*)cell;
@end



@interface TLSwipeForOptionsCell : UITableViewCell
@property int buttonCount;
@property id infoData;
@property NSIndexPath* indexPath;
@property (nonatomic, weak) id<TLSwipeForOptionsCellDelegate> delegate;
@property (nonatomic, weak) UIScrollView *scrollView;

@property (nonatomic, weak) UIView *scrollViewContentView;      //The cell content (like the label) goes in this view.
@property (nonatomic, weak) UIView *scrollViewButtonView;       //Contains our two buttons

@property (nonatomic, weak) UILabel *scrollViewLabel;
@end
