//
//  EOMyOilsShareDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 2/24/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EOMyOilsShareDetailViewController.h"
#import "AppDelegate.h"
#import "EOMyOilsViewController.h"

@interface EOMyOilsShareDetailViewController()
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropCount;
@property (weak, nonatomic) IBOutlet UILabel *dropUnit;
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property AppDelegate* appDelegate;

@end
@implementation EOMyOilsShareDetailViewController
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}
-(void)viewDidLoad
{
    _appDelegate = [[UIApplication sharedApplication]delegate];
    
    CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
    
    
    NSLog(@"%@",_dataArr);
    
    _aboutLabel.text =  _dataArr[3];
    
    if ([_aboutLabel.text length] == 0)
        _aboutLabel.text = @"No notes";
    
    CGSize expectedLabelSize = [_aboutLabel.text sizeWithFont:_aboutLabel.font constrainedToSize:maximumLabelSize lineBreakMode:_aboutLabel.lineBreakMode];
    
    CGRect newFrame = _aboutLabel.frame;
    
    newFrame.size.height = expectedLabelSize.height;
    _aboutLabel.frame = newFrame;

    NSString* text;
    if ([_dataArr[1] intValue] == 1) text = @"Bottles";
    else text = @"Drops";
    
    [_dropUnit setText:text];
    
    [_contactName setText:_dataArr[2]];
    
    [_dropCount setText:[NSString stringWithFormat:@"%02d", [_dataArr[0] intValue]]];
    
    CGRect oldFrame = newFrame;
    
    newFrame = _deleteButton.frame;
    newFrame.origin.y = oldFrame.origin.y + oldFrame.size.height + 20;
    
    _deleteButton.frame = newFrame;
    
    [_mainScrollView setContentSize:CGSizeMake(_mainScrollView.contentSize.width, newFrame.size.height + newFrame.origin.y + 50)];
}

- (IBAction)onDeleteAction:(id)sender
{
    _appDelegate = [[UIApplication sharedApplication]delegate];
    NSMutableDictionary *_dataDict = [[NSMutableDictionary alloc]initWithDictionary:[_appDelegate.myOilsShare objectForKey:_selectedIndex]];
  
    
    NSArray *arrayAllKeys = [_dataDict allKeys];
    NSString *strKeys = [arrayAllKeys objectAtIndex:_indexPathRow];
    
    [[_appDelegate.myOilsShare objectForKey:_selectedIndex] removeObjectForKey:strKeys];
    
    //[_appDelegate.myOilsShare removeObjectForKey:_selectedIndex];
    
    [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOilsShare forKey:@"_myOilsShare"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
