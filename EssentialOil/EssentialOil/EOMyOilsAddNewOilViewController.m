//
//  EOMyOilsAddNewOilViewController.m
//  EssentialOil
//
//  Created by Steve on 2/23/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "AppDelegate.h"
#import "EOMyOilsAddNewOilViewController.h"
#import "EOMyOilsViewController.h"
#import "EOMyOilsShareViewController.h"
#import "SVProgressHUD.h"

@interface EOMyOilsAddNewOilViewController()
@property (weak, nonatomic) IBOutlet UITableView *suggestionTableView;
@property (weak, nonatomic) IBOutlet UITextField *oilNameTextField;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *stockCountLabel;
@property NSMutableArray* wordsArr, *flagArr, *indexArr;
@property AppDelegate* appDelegate;
@property NSString* selectedIndex;
@end
@implementation EOMyOilsAddNewOilViewController
-(void)viewDidLoad
{
 
    [super viewDidLoad];
    
    _selectedIndex = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:_oilNameTextField];
    
    UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onViewTapped:)];
    recognizer.cancelsTouchesInView = NO;
    recognizer.numberOfTapsRequired = 1;
    
    //[_mainView addGestureRecognizer:recognizer];
    _wordsArr = [[NSMutableArray alloc]init];
    _flagArr = [[NSMutableArray alloc]init];
    _indexArr = [[NSMutableArray alloc]init];
    _appDelegate = [[UIApplication sharedApplication]delegate];
    
    int c = 0;
    for (NSArray* a in _appDelegate.oilNames)
        if ([a count] > 7 && [a[7] integerValue] == 1) c++;
        else
        {
            [_wordsArr addObject:a[1]];
            [_flagArr addObject:@"1"];
            [_indexArr addObject:[NSString stringWithFormat:@"%d",
                                  c + 1]];
            c++;
        }
    c = 0;
    
    for (NSArray* b in _appDelegate.myBlends)
        if ([b count] > 3 && [b[3] intValue] == 1) c--;
        else {
            [_wordsArr addObject:b[1]];
            [_flagArr addObject:@"2"];
            [_indexArr addObject:[NSString stringWithFormat:@"%d", c - 1]];
            c--;
        }

    
    NSLog(@"%@", _wordsArr);
    [_suggestionTableView setHidden:YES];
    _suggestionTableView.layer.borderWidth = 1;
    _suggestionTableView.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1] CGColor];
    _suggestionTableView.layer.cornerRadius = 4;
    _suggestionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_oilNameTextField becomeFirstResponder];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (touch.view ==_suggestionTableView) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

-(void)onViewTapped:(id)sender
{
    [_suggestionTableView setHidden:YES];
    //[_oilNameTextField resignFirstResponder];
}

- (void)textFieldDidChange :(NSNotification *)notif
{
    UITextField * textfield = (UITextField*)notif.object;
    NSString * text = textfield.text;
    NSLog(@"%@", text);
    [_suggestionTableView setHidden:NO];
    [_suggestionTableView reloadData];
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[_wordsArr[indexPath.row] lowercaseString] hasPrefix:[_oilNameTextField.text lowercaseString]])
        return 30;
    return 0;
}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}
- (IBAction)onStockMInus:(id)sender {
    [self onViewTapped:nil];
    if ([_stockCountLabel.text integerValue] == 1) return;
    _stockCountLabel.text = [NSString stringWithFormat:@"%02d", [_stockCountLabel.text intValue] - 1];
}
- (IBAction)onStockPlus:(id)sender {
    [self onViewTapped:nil];
    if ([_stockCountLabel.text integerValue] == 999) return;
    
    _stockCountLabel.text = [NSString stringWithFormat:@"%02d", [_stockCountLabel.text intValue] + 1];
}
- (IBAction)onAddOil:(id)sender {
    
    _appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSLog(@"%@",_appDelegate.myOils);
    
    NSMutableDictionary *_myOils = [[NSMutableDictionary alloc]initWithDictionary:_appDelegate.myOils];
    
    NSString* text = [_oilNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([text length] == 0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Empty Oil Name?!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }
    
    int flag = 0;
    int index = -1;
    
    for (int i = 0; i < [_wordsArr count]; i++)
        if ([_wordsArr[i] isEqualToString:text])
        {
            flag = [_flagArr[i] intValue];
            index = i;
        }
    
    if (flag)
    {
        id value = [_myOils objectForKey:_indexArr[index]];
        
        if (value == nil) value = @"0";
        else value = value[0];
        [_myOils setObject:@[[NSString stringWithFormat:@"%d",[_stockCountLabel.text intValue] + [value intValue]]]  forKey:_indexArr[index]];

        _selectedIndex = _indexArr[index];
    }
    else
    {
        NSArray* arrKeys = [_myOils allKeys];
        int count = 0;
        for (NSString* key in arrKeys)
        {
            if ([key hasPrefix:@"N_"])
            {
                count++;
                if ([[_myOils objectForKey:key][1] caseInsensitiveCompare:_oilNameTextField.text] == NSOrderedSame)
                {
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Already existing oil" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
                    return;
                }
            }
        }
        id value = [_myOils objectForKey:[NSString stringWithFormat:@"N_%d", count]];
        
        if (value == nil) value = @"0";
        else value = value[0];
        [_myOils setObject:@[[NSString stringWithFormat:@"%d",[_stockCountLabel.text intValue] + [value intValue]], _oilNameTextField.text]  forKey:[NSString stringWithFormat:@"N_%d", count]];
        _selectedIndex =[NSString stringWithFormat:@"N_%d", count];
    }
    
    _appDelegate.myOils = _myOils;
    
    [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOils forKey:@"_myOils"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
//    NSMutableDictionary *_myOils = [[NSMutableDictionary alloc]init];
//    _myOils = [[NSUserDefaults standardUserDefaults] objectForKey:@"_myOils"];
//    
//    _appDelegate = [[UIApplication sharedApplication] delegate];
//    _appDelegate.myOils = [[NSMutableDictionary alloc] initWithDictionary:_myOils];
    [_appDelegate saveOilData:_myOils];
    
    NSString *strMsg = @"Oil Added";
    [SVProgressHUD showSuccessWithStatus:strMsg];
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    //[textField resignFirstResponder];
    [_suggestionTableView setHidden:YES];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_suggestionTableView setHidden:YES];
    return  YES;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell_Suggestion";
    
    UITableViewCell *cell = [_suggestionTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UILabel* label = ((UILabel*)[cell viewWithTag:10]);
    if ([[_wordsArr[indexPath.row] lowercaseString] hasPrefix:[_oilNameTextField.text lowercaseString]])
        [label setText:_wordsArr[indexPath.row]];
    else [label setText:@""];
    
    return cell;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_suggestionTableView setHidden:NO];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_wordsArr count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setHidden:YES];
    [_oilNameTextField setText:_wordsArr[indexPath.row]];
}
- (IBAction)onShare:(id)sender {
    if ([_selectedIndex length] == 0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"You didn't add oil yet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
        return;
    }
    EOMyOilsShareViewController* vc = (EOMyOilsShareViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myoilsshare"];
    vc.selectedIndex = _selectedIndex;
    [self.navigationController pushViewController:vc animated:YES];
    
}
@end
