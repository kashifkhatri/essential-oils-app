//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EOMyNotesDetailViewController.h"
#import "EOMyOilsViewController.h"
#import "EOMyNotesViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "SVProgressHUD.h"

@interface EOMyNotesViewController ()
@property (weak, nonatomic) IBOutlet UITabBarItem *barButton1;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property AppDelegate* appDelegate;
@property NSMutableArray* keys;
@property NSMutableArray* blendsNotes, *commonUsesNotes, *contactNotes, *myNotes;
@end
@implementation EOMyNotesViewController
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadTableData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_barButton1 setImage:[UIImage imageNamed:@"ES_IconCommonUses30"]];
    // Do any additional setup after loading the view.
    _appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
  

}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([[arrViewControllers objectAtIndex:i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:[arrViewControllers objectAtIndex:i] animated:YES];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4; //oilIndexFavourites   , *commonUsesFavourites, *blendsFavourites, *contactFavourites, *noteFavourites;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
        {
            return [_blendsNotes count];
        }
        case 1:
        {
            return [_commonUsesNotes count];
        }
        case 2:
        {
            return [_contactNotes count];
        }
        case 3:
        {
            return [_myNotes count];
            break;
        }
    }
    return 0;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return @"Blends";
        case 1:
            return @"Common Uses ";
        case 2:
            return @"Contacts ";
        case 3:
            return @"Notes ";
    }
    return @"";
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    header.backgroundColor = [UIColor clearColor];
    header.textLabel.backgroundColor = [UIColor clearColor];
}


/*
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_keys count];
}
 */

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            if ([_blendsNotes count] == 0) return 0;
            break;
        case 1:
            if ([_commonUsesNotes count] == 0) return 0;
            break;
        case 2:
            if ([_contactNotes count] == 0) return 0;
            break;
        case 3:
            if ([_myNotes count] == 0) return 0;
            break;
    }
    return 40;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *text;
    
    switch (indexPath.section)
    {
        case 0:
            text = [_appDelegate.oilNotes objectForKey:[_blendsNotes objectAtIndex:indexPath.row]][0];
            break;
        case 1:
            text = [_appDelegate.oilNotes objectForKey:[_commonUsesNotes objectAtIndex:indexPath.row]][0];
            break;
        case 2:
            text = [_appDelegate.oilNotes objectForKey:[_contactNotes objectAtIndex:indexPath.row]][0];
            break;
        case 3:
            text = [_appDelegate.oilNotes objectForKey:[_myNotes objectAtIndex:indexPath.row]][0];
            break;
    }
    
    cell.textLabel.text = text;
    //@"asdF";//[_appDelegate.oilNotes objectForKey:_keys[indexPath.row]][0];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.delegate = self;
    cell.indexPath = indexPath;
    
    cell.buttonCount = 3;
    
    NSArray* imageButton = @[@"ES_SettingIconBack.png", @"ES_SettingIconFavorite.png", //@"ES_SettingIconNotepad.png",
                             @"ES_SettingIconTrash.png"];
    
    [cell.scrollViewButtonView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1]];
    
    if (indexPath.row == 3)
    {
        int s = 0;
        s = 1;
    }
    for (int i = 0; i < 3; i++)
    {
        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        moreButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
        int height = 49 - 20;
        if (height < 0) height=0;
        moreButton.frame = CGRectMake(49 * i + 10, 10, 49 - 20, height);
        [moreButton setImage:[UIImage imageNamed:imageButton[i]] forState:UIControlStateNormal];
        [moreButton setTag:i + indexPath.row * 5];
        [moreButton addTarget:self action:@selector(userPressedMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.scrollViewButtonView addSubview:moreButton];
    }
    
    
    return cell;
}


-(void)userPressedMoreButton:(id)sender
{
    int buttonIndex = ((UIButton*)sender).tag % 5;
    int rowIndex = ((UIButton*)sender).tag / 5;
    
    switch (buttonIndex)
    {
        case 0:
            [[NSNotificationCenter defaultCenter]postNotificationName:TLSwipeForOptionsCellEnclosingTableViewDidBeginScrollingNotification object:nil];
            break;
        case 1:
        {
            NSLog(@"%@",_keys);
            if ([_appDelegate.noteFavourites objectForKey:[_keys objectAtIndex:rowIndex]])
            {
                [_appDelegate.noteFavourites removeObjectForKey:[_keys objectAtIndex:rowIndex]];
                [SVProgressHUD showSuccessWithStatus:@"Removed From Favorites!"];
            }
            else
            {
                [_appDelegate.noteFavourites setObject:@"1" forKey:[_keys objectAtIndex:rowIndex]];
                [SVProgressHUD showSuccessWithStatus:@"Added To Favorites!"];
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.noteFavourites forKey:@"_noteFavourites"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_mainTableView reloadData];
            
            NSLog(@"%@",_appDelegate.noteFavourites);
            
            break;
        }
        
        case 2:
        {
            int noteID = [[_keys objectAtIndex:rowIndex] intValue];
            
            [_appDelegate.oilNotes removeObjectForKey:[NSString stringWithFormat:@"%d", noteID]];
            
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.oilNotes forKey:@"_oilNotes"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self reloadTableData];
            
        }
    }
}


-(void)reloadTableData
{
    NSLog(@"%@",_appDelegate.oilNotes);
    
    _keys = [[NSMutableArray alloc]initWithArray:[_appDelegate.oilNotes allKeys]];
    
    NSLog(@"%@",_keys);
    
    _blendsNotes = [[NSMutableArray alloc] init];
    _commonUsesNotes = [[NSMutableArray alloc] init];
    _contactNotes = [[NSMutableArray alloc] init];
    _myNotes = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [_keys count]; i++) {
        if ([[_keys objectAtIndex:i] integerValue] >=0 && [[_keys objectAtIndex:i] integerValue] < 500 ) {
            [_myNotes addObject:[_keys objectAtIndex:i]];
        }
        else if ([[_keys objectAtIndex:i] integerValue] >=500 && [[_keys objectAtIndex:i] integerValue] < 1000 ) {
            [_blendsNotes addObject:[_keys objectAtIndex:i]];
        }
        else if ([[_keys objectAtIndex:i] integerValue] >=1000 && [[_keys objectAtIndex:i] integerValue] < 2000 ) {
            [_commonUsesNotes addObject:[_keys objectAtIndex:i]];
        }
        else if ([[_keys objectAtIndex:i] integerValue] > 19999) {
            [_contactNotes addObject:[_keys objectAtIndex:i]];
        }
    }
    
    _blendsNotes = [[NSMutableArray alloc] initWithArray:[self sortByAscending:_blendsNotes]];
    _commonUsesNotes = [[NSMutableArray alloc] initWithArray:[self sortByAscending:_commonUsesNotes]];
    _contactNotes = [[NSMutableArray alloc] initWithArray:[self sortByAscending:_contactNotes]];
    _myNotes = [[NSMutableArray alloc] initWithArray:[self sortByAscending:_myNotes]];
    
    [_mainTableView reloadData];
}

- (NSMutableArray *) sortByAscending:(NSArray *)keys
{
    NSMutableDictionary *arraySort = [[NSMutableDictionary alloc] init];
    
    for (int i =0; i <[keys count]; i++) {
        NSArray *dic = [_appDelegate.oilNotes objectForKey:[keys objectAtIndex:i]];
        NSLog(@"%@",[dic objectAtIndex:0]);
        
        NSString *text = [[dic objectAtIndex:0] lowercaseString];
        
        [arraySort setObject:text forKey:[keys objectAtIndex:i]];
    }
    
    NSLog(@"arraySort: %@",arraySort);
    
    NSMutableArray *_tempKeys = [[NSMutableArray alloc] initWithArray:[arraySort keysSortedByValueUsingSelector:@selector(compare:)]];
    NSLog(@"_tempKeys: %@",_tempKeys);
    
    return _tempKeys;
}

-(void)onSelectCell:(TLSwipeForOptionsCell*)cell
{
    int noteID = 0;
    
    NSLog(@"%@",_blendsNotes);
    
    switch (cell.indexPath.section)
    {
        case 0:
        {
            noteID = [[_blendsNotes objectAtIndex:cell.indexPath.row] intValue];
            break;
        }
        case 1:
            noteID = [[_commonUsesNotes objectAtIndex:cell.indexPath.row] intValue];
            break;
        case 2:
            noteID = [[_contactNotes objectAtIndex:cell.indexPath.row] intValue];
            break;
        case 3:
            noteID = [[_myNotes objectAtIndex:cell.indexPath.row] intValue];
            break;
    }

    
    EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];

    viewController.parentVCX = self;
    viewController.noteID = noteID;
    //[[_keys objectAtIndex:cell.indexPath.row] intValue];

    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)onNavMyOils:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_myoils" object:nil];
}
- (IBAction)onMyContacts:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_mycontacts" object:nil];
    
}
- (IBAction)onMyBlends:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_myblends" object:nil];
    
}
- (IBAction)onNewNote:(id)sender {
    EOMyNotesDetailViewController* viewController = (EOMyNotesDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
    viewController.parentVCX = self;
    viewController.noteID = -1;
    
    [self.navigationController presentViewController:viewController animated:YES completion:nil];

}
@end
