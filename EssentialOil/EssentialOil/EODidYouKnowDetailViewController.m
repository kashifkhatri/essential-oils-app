//
//  EODidYouKnowDetailViewController.m
//  EssentialOil
//
//  Created by Steve on 1/8/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//

#import "EODidYouKnowDetailViewController.h"
#import "EODidyouknowViewController.h"
#import "AppDelegate.h"

@interface EODidYouKnowDetailViewController ()
@property AppDelegate* appDelegate;
@end

@implementation EODidYouKnowDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _appDelegate = [[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view.
    [self updateText];
}
- (IBAction)onBackTip:(id)sender {
    _selectedIndex --;
    if (_selectedIndex < 0) _selectedIndex = [_appDelegate.tips count] - 1;
    [self updateText];
}
- (IBAction)onNextTip:(id)sender {
    _selectedIndex ++;
    if (_selectedIndex >= [_appDelegate.tips count]) _selectedIndex = 0;
    [self updateText];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}
- (IBAction)onHome:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        [_parentVCX onHome:nil];
    }];
}
- (IBAction)onBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onShare:(id)sender {

    NSString *textToShare = _appDelegate.tips[_selectedIndex][1];
    
    //NSURL *myWebsite = [NSURL URLWithString:@"http://www.apple.com/"];
    
    NSArray *objectsToShare = @[textToShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   
                                   ];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    

}

-(void)updateText
{
    UILabel* label = (UILabel*)[self.view viewWithTag:20];
    
    [label setText:_appDelegate.tips[_selectedIndex][1]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
