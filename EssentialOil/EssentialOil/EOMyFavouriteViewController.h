#import <UIKit/UIKit.h>
#import "TLSwipeForOptionsCell.h"

@interface EOMyFavouriteViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, TLSwipeForOptionsCellDelegate,UISearchBarDelegate>
-(IBAction)onHome:(id)sender;
@end
