//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "SVProgressHUD.h"
#import "EOMyBlendsViewController.h"
#import "EOMyBlendsDetailViewController.h"
#import "EOMyNotesViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "EOMyOilsViewController.h"
#import "EOMyNotesDetailViewController.h"
#import "AppDelegate.h"
@interface EOMyBlendsViewController ()
@property (weak, nonatomic) IBOutlet UITabBarItem *barButton1;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property AppDelegate* appDelegate;
@end

@implementation EOMyBlendsViewController

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_mainTableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_barButton1 setImage:[UIImage imageNamed:@"ES_IconCommonUses30"]];
    _appDelegate = [[UIApplication sharedApplication]delegate];
    
}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([arrViewControllers[i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:arrViewControllers[i] animated:YES];
            break;
        }
    }
}
- (IBAction)onCreateNewBlends:(id)sender {
    EOMyBlendsDetailViewController* viewcontroller = (EOMyBlendsDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"myblendsdetailview"];
    viewcontroller.selectedIndex = -1;
    [self.navigationController pushViewController:viewcontroller animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%@", _appDelegate.myBlends);
    if ([_appDelegate.myBlends[indexPath.row] count] > 3 && [_appDelegate.myBlends[indexPath.row][3] intValue] == 1)
        return 0;
    return 49;
}
-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_appDelegate.myBlends count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = _appDelegate.myBlends[indexPath.row][1];
    cell.delegate = self;
    cell.indexPath = indexPath;
    cell.clipsToBounds = YES;
    cell.buttonCount = 4;
    for (UIView* view in cell.scrollView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    for (UIView* view in cell.scrollViewButtonView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    NSArray* imageButton = @[@"ES_SettingIconBack.png", @"ES_SettingIconFavorite.png", @"ES_SettingIconNotepad.png",
                             @"ES_SettingIconTrash.png"];
    
    [cell.scrollViewButtonView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1]];
    
    if (indexPath.row == 3)
    {
        int s = 0;
        s = 1;
    }
    for (int i = 0; i < 4; i++)
    {
        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        moreButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
        int height = 49 - 20;
        if (height < 0) height=0;
        moreButton.frame = CGRectMake(49 * i + 10, 10, 49 - 20, height);
        [moreButton setImage:[UIImage imageNamed:imageButton[i]] forState:UIControlStateNormal];
        [moreButton setTag:i + indexPath.row * 5];
        [moreButton addTarget:self action:@selector(userPressedMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.scrollViewButtonView addSubview:moreButton];
    }
    int _buttonCount = 4;
    
    int leftPos = 0 + [[UIScreen mainScreen]bounds].size.width - IMAGE_SIZE - 10 + kCatchWidth;
    
    
    id note = [_appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%d", 500 + [_appDelegate.myBlends[indexPath.row][0] intValue]]];
    
    if ([_appDelegate.blendsFavourites objectForKey:_appDelegate.myBlends[indexPath.row][0]])
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, IMAGE_SIZE, IMAGE_SIZE)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_hearts.png"]];
        [cell.scrollView addSubview:noteImageView];
        leftPos -= IMAGE_SIZE + 15;
        cell.scrollView.clipsToBounds = YES;
    }
    if (note)
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 12, IMAGE_SIZE, IMAGE_SIZE)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_IconNotepad.png"]];
        [cell.scrollView addSubview:noteImageView];
        cell.scrollView.clipsToBounds = YES;
        
        //[noteImageView addGestureRecognizer:rcongicne];
    }

    
    return cell;
}
-(void)userPressedMoreButton:(id)sender
{
    int buttonIndex = ((UIButton*)sender).tag % 5;
    int rowIndex = ((UIButton*)sender).tag / 5;
    
    switch (buttonIndex)
    {
        case 0:
            [[NSNotificationCenter defaultCenter]postNotificationName:TLSwipeForOptionsCellEnclosingTableViewDidBeginScrollingNotification object:nil];
            break;
        case 1:
        {
            NSLog(@"%@", _appDelegate.blendsFavourites);
            NSLog(@"%@", _appDelegate.myBlends);
            if ([_appDelegate.blendsFavourites objectForKey:_appDelegate.myBlends[rowIndex][0]])
            {
                [_appDelegate.blendsFavourites removeObjectForKey:_appDelegate.myBlends[rowIndex][0]];
                [SVProgressHUD showSuccessWithStatus:@"Removed From Favorites!"];
            }
            else
            {
                [_appDelegate.blendsFavourites setObject:@"1" forKey:_appDelegate.myBlends[rowIndex][0]];
                [SVProgressHUD showSuccessWithStatus:@"Added To Favorites!"];
            }
            

            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.blendsFavourites forKey:@"_blendsFavourites"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_mainTableView reloadData];
            
            break;
        }
        case 2:
        {
            EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];

            viewController.noteID = 500 + [_appDelegate.myBlends[rowIndex][0] intValue];
            viewController.parentVCX = self;
            viewController.stringTitle = _appDelegate.myBlends[rowIndex][1];
            [self presentViewController:viewController animated:YES completion:nil];
            break;
        }
        case 3:
        {
            NSArray* a = _appDelegate.myBlends[rowIndex];
            
            _appDelegate.myBlends[rowIndex] = @[a[0], a[1], a[2], @"1"];

            [_mainTableView reloadData];
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myBlends forKey:@"_myBlends"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [SVProgressHUD showSuccessWithStatus:@"Successfully Removed!"];
        }
    }
}
-(void)onSelectCell:(TLSwipeForOptionsCell*)cell
{
    EOMyBlendsDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myblendsdetailview"];
    viewController.appDelegate = _appDelegate;
    viewController.selectedIndex = cell.indexPath.row;
    viewController.parentVCX = self;
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)onNavMyOils:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_myoils" object:nil];
}
- (IBAction)onMyNotes:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_mynotes" object:nil];
    
}
- (IBAction)onMyContacts:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_mycontacts" object:nil];
    
}
@end
