//
//  EOOilIndexViewController.m
//  EssentialOil
//
//  Created by Steve on 1/5/15.
//  Copyright (c) 2015 JJ. All rights reserved.
//
#import "EOMyContactsViewController.h"
#import "TLSwipeForOptionsCell.h"
#import "EOMyContactsDetailViewController.h"
#import "EOMyNotesViewController.h"
#import "EOMyOilsViewController.h"
#import "EOMyNotesDetailViewController.h"
#import "SVProgressHUD.h"

@interface EOMyContactsViewController ()
@property (weak, nonatomic) IBOutlet UITabBarItem *barButton1;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property AppDelegate* appDelegate;
@property NSMutableArray* keys;
@property int buttonCount;
@end

@implementation EOMyContactsViewController
-(void)viewDidAppear:(BOOL)animated
{

    [super viewDidAppear:animated];
    [self reloadTable];
}
- (void)viewDidLoad {
    [super viewDidLoad];
        _buttonCount = 4;
    _appDelegate = [[UIApplication sharedApplication]delegate];
    [_barButton1 setImage:[UIImage imageNamed:@"ES_IconCommonUses30"]];
    // Do any additional setup after loading the view.
    [self reloadTable];

}
- (IBAction)onHome:(id)sender {
    NSArray* arrViewControllers = [self.navigationController viewControllers];
    for (int i = 0; [arrViewControllers count] >= i; i++)
    {
        if ([[arrViewControllers objectAtIndex:i] isKindOfClass:[EOMyOilsViewController class]])
        {
            [self.navigationController popToViewController:[arrViewControllers objectAtIndex:i] animated:YES];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)reloadTable
{
    //_appDelegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"%@",_appDelegate.myContacts);
    NSLog(@"%@",_appDelegate.oilNotes);
    NSLog(@"%@",_appDelegate.myNotes);
    [_mainTableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_appDelegate.myContacts count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_appDelegate.myContacts[indexPath.row] count] == 3 && [@"1" isEqualToString:_appDelegate.myContacts[indexPath.row]])
        return 0;
    return 49;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString*cellId = @"Cell";
    TLSwipeForOptionsCell *cell = (TLSwipeForOptionsCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if ([self tableView:tableView heightForRowAtIndexPath:indexPath] == 0) return cell;
    
    if (cell == nil) {
        cell = [[TLSwipeForOptionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    cell.clipsToBounds = YES;
    cell.buttonCount = 4;
    cell.textLabel.text = _appDelegate.myContacts[indexPath.row][1][0];
    cell.indexPath = indexPath;
    
    int leftPos = 0 + [[UIScreen mainScreen]bounds].size.width - IMAGE_SIZE - 10 + kCatchWidth;
    
    
    cell.scrollView.backgroundColor = [UIColor blackColor];
    
    cell.buttonCount =4;
    
    for (UIView* view in cell.scrollView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    
    cell.delegate = self;
    
   // [cell.scrollView setFrame:CGRectMake(0, 0, kCatchWidth, CGRectGetHeight(cell.bounds))];
    NSLog(@"%@",NSStringFromCGRect(cell.scrollView.frame));
    
    if ([_appDelegate.oilNotes objectForKey:[NSString stringWithFormat:@"%ld", 20000 + indexPath.row]])
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 7, 25, 27)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_IconNotepad.png"]];
        [cell.scrollView addSubview:noteImageView];
        leftPos -= IMAGE_SIZE + 15;
        cell.scrollView.clipsToBounds = YES;
        NSLog(@"%@",NSStringFromCGRect(noteImageView.frame));
    }
    
    if ([_appDelegate.contactFavourites objectForKey:_appDelegate.myContacts[indexPath.row][0]])
    {
        UIImageView* noteImageView = [[UIImageView alloc]initWithFrame:CGRectMake(leftPos, 7, IMAGE_SIZE, IMAGE_SIZE)];
        noteImageView.tag = 1003;
        noteImageView.userInteractionEnabled = NO;
        [noteImageView setImage:[UIImage imageNamed:@"ES_hearts.png"]];
        [cell.scrollView addSubview:noteImageView];
        leftPos -= IMAGE_SIZE + 15;
        cell.scrollView.clipsToBounds = YES;
    }
    

    
    NSArray* imageButton = @[@"ES_SettingIconBack.png", @"ES_SettingIconFavorite.png", @"ES_SettingIconNotepad.png",
                             @"ES_SettingIconTrash.png"];
    
    [cell.scrollViewButtonView setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1]];
    
    if (indexPath.row == 3)
    {
        int s = 0;
        s = 1;
    }
    for (int i = 0; i < 4; i++)
    {
        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        moreButton.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
        int height = 49 - 20;
        if (height < 0) height=0;
        moreButton.frame = CGRectMake(49 * i + 10, 10, 49 - 20, height);
        [moreButton setImage:[UIImage imageNamed:imageButton[i]] forState:UIControlStateNormal];
        [moreButton setTag:i + indexPath.row * 5];
        [moreButton addTarget:self action:@selector(userPressedMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.scrollViewButtonView addSubview:moreButton];
    }
    
    
    
    
    return cell;
}

-(void)onSelectCell:(TLSwipeForOptionsCell*)cell
{
    EOMyContactsDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mycontactsdetail"];

    viewController.parentVCX = self;
    viewController.selectedIndex = cell.indexPath.row;
    
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}
- (IBAction)onNavMyOils:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_myoils" object:nil];
}
- (IBAction)onMyNotes:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_mynotes" object:nil];

}
- (IBAction)onMyBlends:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"menu_myblends" object:nil];

}


-(void)userPressedMoreButton:(id)sender
{
    int buttonIndex = ((UIButton*)sender).tag % 5;
    int rowIndex = ((UIButton*)sender).tag / 5;
    
    switch (buttonIndex)
    {
        case 0:
            [[NSNotificationCenter defaultCenter]postNotificationName:TLSwipeForOptionsCellEnclosingTableViewDidBeginScrollingNotification object:nil];
            break;
        case 1:
        {
            if ([_appDelegate.contactFavourites objectForKey:_appDelegate.myContacts[rowIndex][0]])
            {
                [_appDelegate.contactFavourites removeObjectForKey:_appDelegate.myContacts[rowIndex][0]];
                [SVProgressHUD showSuccessWithStatus:@"Removed From Favorites!"];
            }
            else
            {
                [_appDelegate.contactFavourites setObject:@"1" forKey:_appDelegate.myContacts[rowIndex][0]];
                [SVProgressHUD showSuccessWithStatus:@"Added To Favorites!"];
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.contactFavourites forKey:@"_contactFavourites"];
            [[NSUserDefaults standardUserDefaults]synchronize];

            
            [self reloadTable];
            [_mainTableView reloadData];
            
            NSLog(@"%@",_appDelegate.contactFavourites);
            
            break;
        }
        case 2:
        {
            EOMyNotesDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mynotesetail"];
            
            @try {
                viewController.noteID = 20000 + [_appDelegate.myContacts[rowIndex][0] intValue];
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
            
            viewController.parentVCX = self;
            viewController.stringTitle = _appDelegate.myContacts[rowIndex][1][0];
            [self presentViewController:viewController animated:YES completion:nil];
            break;
        }
        case 3:
        {
            NSArray* a = _appDelegate.myContacts[rowIndex];
            _appDelegate.myContacts[rowIndex] = @[a[0], a[1], @"1"];
            
            NSLog(@"%@",_appDelegate.myContacts);
            NSLog(@"%@",_appDelegate.myOilsShare);
            
            NSArray *arrayKeys = [_appDelegate.myOilsShare allKeys];
            
            for (int i =0 ; i < [arrayKeys count]; i++) {
                NSString *keyId = [arrayKeys objectAtIndex:i];

                NSMutableDictionary *_dataDict = [[NSMutableDictionary alloc]initWithDictionary:[_appDelegate.myOilsShare objectForKey:keyId]];
                
                NSArray *allKeys = [_dataDict allKeys];
                
                if ([allKeys count] > 0) {

                    NSLog(@"%ld",[[allKeys objectAtIndex:0] integerValue]);
                    NSLog(@"%ld",[[[_appDelegate.myContacts objectAtIndex:rowIndex] objectAtIndex:0] integerValue]);
                    
                    NSLog(@"%@",[allKeys objectAtIndex:rowIndex]);
                    
                    [[_appDelegate.myOilsShare objectForKey:keyId] removeObjectForKey:[NSString stringWithFormat:@"%d",rowIndex]];
                }
                
            }
            
            [_appDelegate.myContacts removeObjectAtIndex:rowIndex];
            [_mainTableView reloadData];
            
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myOilsShare forKey:@"_myOilsShare"];
            [[NSUserDefaults standardUserDefaults]setObject:_appDelegate.myContacts forKey:@"_myContacts"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [SVProgressHUD showSuccessWithStatus:@"Successfully Removed!"];
            
            NSLog(@"%@",_appDelegate.myOilsShare);
            NSLog(@"%@",_appDelegate.myContacts);
        }
    }
}

- (IBAction)onNewContact:(id)sender {
    EOMyContactsDetailViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mycontactsdetail"];
    
    viewController.parentVCX = self;
    viewController.selectedIndex = -1;
    
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}

@end
